<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$route['default_controller'] = "main";
$route['contacte'] = "main/contacto";
$route['food'] = "foods/front";
$route['food/(:any)-(:any)'] = "foods/front/read/$1";
$route['food/(:any)'] = "foods/front/$1";
$route['beverage'] = "beverages/front";
$route['beverage/clases_funciones/(:any)'] = "beverages/front/clases_funciones/$1";
$route['beverage/(:any)-(:any)'] = "beverages/front/read/$1";
$route['beverage/(:any)'] = "beverages/front/$1";
$route['404_override'] = 'main/error404';
$route['403_override'] = 'main/error403';


/* End of file routes.php */
/* Location: ./application/config/routes.php */
