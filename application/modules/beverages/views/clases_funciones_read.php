<?= $this->load->view('includes/headerMain') ?>
<div id="wrapper">    
    <div class="max" id="content">
    <div class="resultsList col-xs-12 col-sm-12 col-md-8 col-md-offset-1">
        <div class="singleTop">
            <div id="carouselFull" class="carousel slide gallery" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    <li data-target="#carouselFull" data-slide-to="0"class="active" ></li>
                </ol>
                <div class="carousel-inner">
                    <a href="<?= base_url('img/tipos_funciones/'.$detail->foto) ?>" data-fancybox-group="slideshow" class="galleryItem item active" style="background-image: url(<?= base_url('img/tipos_funciones/'.$detail->foto) ?>)"></a>
                </div>
                <a class="left carousel-control" href="#carouselFull" role="button" data-slide="prev"><span class="fa fa-chevron-left"></span></a>
                <a class="right carousel-control" href="#carouselFull" role="button" data-slide="next"><span class="fa fa-chevron-right"></span></a>
            </div>

            <div class="summary">                
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div class="summaryItem">                                                        
                            <h1 class="pageTitle">
                                    <?= $detail->tipos_funciones_nombre ?>
                            </h1>
                            <div class="clearfix"></div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

        <div class="description" style="border-bottom: 1px solid black;">
            <div class="entry-content">
                <?= $detail->ficha ?>
                <div class="clearfix"></div>
            </div>
        </div>        
    </div>    
    <div class="resultsList rel col-xs-12 col-sm-12 col-md-3" style="margin-top: 40px;">
        <h4>Productos Relacionados</h4>
        <?php foreach($relacionados->result() as $l): ?>
            <div class="col-xs-12 col-sm-4 col-md-12">
                <a href="<?= site_url('beverage/'.$l->id.'-'.toURL($l->beverage_nombre)) ?>" class="card" id="card-312">
                    <?php if($l->new==1): ?>
                    <div class="new">
                        <div></div>
                        <span>NEW</span>
                    </div>
                    <?php endif ?>
                    <div class="figure">
                        <div class="img" style="background-image:url(<?= base_url('img/beverages/'.$l->miniatura) ?>);"></div>
                        <div class="figCaption"></div>
                        <div class="figView"><span class="icon-eye"></span></div>                            
                    </div>
                    <h2><?= $l->beverage_nombre ?></h2>
                    <div class="cardAddress">
                        
                    </div>
                    <div class="cardAddress" style="text-align:right">
                        <span style="color:red">Leer más</span>
                    </div>
                    <ul class="cardFeat"></ul>
                    <div class="clearfix"></div>
                </a>
            </div>
            <?php endforeach ?>
    </div>
    </div>
</div>