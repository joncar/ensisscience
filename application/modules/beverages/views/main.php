<div id="hero-container">
    <div id="slideshow">
        <div style='background-image: url(<?= base_url('img/uploads/bg-11.jpg') ?>)'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-10.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-9.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-8.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-7.jpg') ?>); display:none;'></div>        
    </div>
    <div class="slideshowShadow"></div>
    <div class="home-header menuontop" id='menu'>
        <div class="col-xs-12" id="homelogo2" style="text-align:center;">
            <a href="<?= site_url() ?>">
                <?= img('img/logo.png','width:40%') ?>
            </a>
        </div>
        <div class="col-lg-3 hidden-xs home-logo osLight" id="homelogo">
            <a href="<?= site_url() ?>">
                <?= img('img/logo.png') ?>
            </a>
        </div>
        
        <?php $this->load->view('includes/fragmentos/menu'); ?>
    </div>
    <div class="home-caption">
        <h1 class="home-title">INGREDIENTES FUNCIONALES PARA BEBIDAS</h1>
        
    </div>
    <div class="search-panel">
        <form class="form-inline" method="get" action="<?= base_url('beverage/lista') ?>">            
            <div class="form-group">
                <input type="search" size='50' class="form-control" id="descripcion" name="descripcion" placeholder="Introduce las palabras separadas por coma" autocomplete="off">
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Tipos</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="tipos_id" value="" checked="checked"><a href="javascript:void(0);">Tipo</a>
                    </li>
                    <?php foreach($tipos->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="tipos_id" value="<?= $d->id ?>"><a href="javascript:void(0);"><?= $d->tipo_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Funciones</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="funciones" value="" checked="checked"><a href="javascript:void(0);">Funciones</a>
                    </li>
                    <?php foreach($funciones->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="funciones" value="<?= $d->url ?>"><a href="javascript:void(0);"><?= $d->beverage_funciones_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Clasificaciones</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="clasificacion_id" value="" checked="checked"><a href="javascript:void(0);">Clasificaciones</a>
                    </li>
                    <?php foreach($clasificacion->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="clasificacion_id" value="<?= $d->id ?>"><a href="javascript:void(0);"><?= $d->clasificacion_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="form-group">
                <input type="submit" id="searchPropertySubmit" class="btn btn-green" value="Buscar">
                <a href="javascript:void(0);" class="btn btn-o btn-white pull-right visible-xs" id="advanced">Búsqueda Avanzada <span class="fa fa-angle-up"></span></a>
            </div>
        </form>
    </div>
</div>
<div class="spotlight" id="page" >
    <div class="s-title osLight">Busque su ingrediente perfecto para su industria</div>
    <div class="s-text osLight">Si no encuentra su ingrediente nosotros le asesoraremos con nuestra amplia experiéncia en el sector</div>
</div>
<div class="page-wrapper no-touch">
    <div class="page-content">

        <div id="post-2" class="post-2 page type-page status-publish hentry">
            <div class="entry-content">
                <h2 class="osLight centered" id="tipos">Tipos</h2>
                <div class="row pb40">
                    <?php foreach($tipos->result() as $d): ?>
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 s-menu-item">
                                <a href="<?= base_url('beverage/lista/') ?>?tipos_id=<?= $d->id ?>">
                                    <span class="<?= $d->icono ?> s-icon"></span>
                                    <div class="s-content">
                                        <h2 class="centered s-main osLight"><?= $d->tipo_nombre ?></h2>
                                        <h3 class="s-sub osLight"><?= $d->resumen ?></h3>
                                    </div>
                                </a>
                            </div>                    
                    <?php endforeach ?>
                </div>
                <h2 class="centered osLight" id="clasificaciones">Clasificaciones - <?= $tipos->row(0)->tipo_nombre ?></h2>
                <div class="row pb40">
                    <?php foreach($clasificacion2->result() as $d): ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <a href="<?= base_url('beverage/lista/') ?>?clasificacion_id=<?= $d->id ?>" class="propWidget-2">
                            <div class="fig">
                                <?= img('img/clasificaciones/'.$d->icono,'',TRUE,'class="scale" alt="" data-scale="best-fill" data-align="center"') ?>
                                <?= img('img/clasificaciones/'.$d->icono,'',TRUE,'class="blur scale" alt="" data-scale="best-fill" data-align="center"') ?>
                                <div class="opac"></div>                                
                                <h3 class="osLight"><?= $d->clasificacion_nombre ?></h3>
                                <div class="address"></div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach ?>
                </div>
                <h2 class="centered osLight">Clasificaciones - <?= $tipos->row(1)->tipo_nombre ?></h2>
                <div class="row pb40">
                    <?php foreach($clasificacion1->result() as $d): ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <a href="<?= base_url('beverage/lista/') ?>?clasificacion_id=<?= $d->id ?>" class="propWidget-2">
                            <div class="fig">
                                <?= img('img/clasificaciones/'.$d->icono,'',TRUE,'class="scale" alt="" data-scale="best-fill" data-align="center"') ?>
                                <?= img('img/clasificaciones/'.$d->icono,'',TRUE,'class="blur scale" alt="" data-scale="best-fill" data-align="center"') ?>
                                <div class="opac"></div>                                
                                <h3 class="osLight"><?= $d->clasificacion_nombre ?></h3>
                                <div class="address"></div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach ?>
                </div>
                <h2 class="centered osLight" id="funciones">Funciones</h2>
                <div class="row pb40">                    
                        <?php for($i=0;$i<4;$i++): if($i<$funciones->num_rows): $d = $funciones->result->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('beverage/clases_funciones') ?>/<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/beverage_funciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->beverage_funciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                    </div>
                <div class="row pb40">
                    <?php for($i=4;$i<8;$i++): if($i<$funciones->num_rows): $d = $funciones->result->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('beverage/clases_funciones') ?>/<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/beverage_funciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->beverage_funciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                </div>
                <div class="row pb40">
                    <?php for($i=8;$i<12;$i++): if($i<$funciones->num_rows): $d = $funciones->result->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('beverage/clases_funciones') ?>/<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/beverage_funciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->beverage_funciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                </div>
                <div class="row pb40">
                    <?php for($i=12;$i<16;$i++): if($i<$funciones->num_rows): $d = $funciones->result->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('beverage/clases_funciones') ?>/<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/beverage_funciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->beverage_funciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>
<div class="row contactenos" style="margin-left:0px; margin-right:0px;">
    <h2 class="centered osLight">Contáctenos</h2>
    <p align='center'>Si tienes alguna duda o quieres preguntar por un producto</p>
    <div class='col-xs-12 col-sm-6 col-sm-offset-3'>
        <form id="formcontact" onsubmit="return sendMessage()" class="contactPageForm">            
            <div class="row">
                <div id="cp_response" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="cp_name">Nombre <span class="text-red">*</span></label>
                        <input id="nombre" name="nombre" placeholder="Ingresa tu nombre" class="form-control" type="text" required="">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="cp_email">Email <span class="text-red">*</span></label>
                        <input id="email" name="email" placeholder="Ingresa tu email" class="form-control" type="email" required="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="cp_message">Mensaje <span class="text-red">*</span></label>
                        <textarea id="mensaje" name="mensaje" placeholder="Ingresa tu mensaje" rows="3" class="form-control" required=""></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <button class="btn btn-green">Enviar mensaje</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<div class='page-wrapper'>
    <h2 class="centered osLight">Nuestros Productos</h2>
    <div class="row pb40">
        <div id="home-testimonials" class="carousel slide carousel-wb mb20" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php $opiniones = $this->db->get_where('opiniones',array('tipo'=>1)); ?>
                <?php foreach($opiniones->result() as $n=>$v): ?>
                    <li data-target="#home-testimonials" data-slide-to="<?= $n ?>" <?php if($n==0): ?>class="active"<?php endif ?> ></li>
                <?php endforeach ?>                
            </ol>
            <div class="carousel-inner">
                <?php foreach($opiniones->result() as $n=>$v): ?>
                    <div class="item <?php if($n==0): ?>active<?php endif ?>">
                        <img src="<?= base_url('img/opiniones/'.$v->foto) ?>" class="home-testim-avatar" alt="<?= $v->nombre ?>">
                        <div class="home-testim">
                            <div class="home-testim-text">
                                <?= $v->comentario ?>
                            </div>
                            <div class="home-testim-name"><?= $v->nombre ?></div>
                        </div>                        
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<div class='page-wrapper' style="padding:40px 0 0px; margin:0 ; width:100%;">
    <?php $coords = maptoarray($this->ajustes->mapa_contacto); $cord = $coords[0].','.$coords[1]; ?>    
    <div id='mapa' style="width:100%; height:300px;"></div>
    <script src="http://maps.googleapis.com/maps/api/js?signed_in=true&v=3.exp&sensor=true"></script>
    <script>
        var mapaContent = document.getElementById('mapa');
        var center = new google.maps.LatLng(<?= $cord ?>);    
        var mapOptions = {
                    zoom: 14,
                    center: center,               
        };
        var map = new google.maps.Map(mapaContent, mapOptions);
        new google.maps.Marker({position: new google.maps.LatLng(<?= $cord ?>),map:map});
    </script>
</div>
<?= $this->load->view('includes/footer') ?>
<script>
    $(document).ready(function(){
        $(window).on('scroll',function(){
            if($(this).scrollTop()>50)
                $("#menu").addClass('menuonbottom').removeClass('menuontop');
            else
                $("#menu").addClass('menuontop').removeClass('menuonbottom');
        });
        
        $("a[href*=#]").click(function(e){
            e.preventDefault();
            var target = $(this).attr('href');
            target = target.split("#");
            target = $("#"+target[1]);
            
            $('html, body').stop().animate({'scrollTop': parseInt(target.offset().top)-30}, 900, 'swing');        
    })
    });
    
    function sendMessage(){
        if($("#nombre").val()!=='' && $("#email").val()!=='' && $("#mensaje").val()!==''){
            var datos = document.getElementById('formcontact');
            var f = new FormData(datos);
            console.log(f);
            $.ajax({
                url:'<?= base_url('main/contacto') ?>',
                data:f,
                cache: false,
                contentType: false,
                processData: false,
                type:'POST',
                success:function(data){
                    alert('Su mensaje ha sido enviado con éxito');
                }
            });
        }else{
            alert('por favor complete los datos, para poder contactarlo.');
        }
        return false;
    }
</script>