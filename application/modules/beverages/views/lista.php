<?php 
    $p = array();
    $gama = '';
    foreach($total->result() as $l){
        foreach($funciones->result() as $f){
            if($gama!=$f->id && $l->{$f->url}==1){
                $p[$f->id][] = $f->id;
            }            
        }
    }
?>
<?php $this->load->view('includes/headerMain'); ?>
<div id="wrapper">
    <div id="content" class='max'>
        <?= $this->load->view('includes/searchbox') ?>
        <div class="resultsList">
            <h1 class="pull-left">
                Resultados <span id="etiquetas"></span>
            </h1>
            <div class="clearfix"></div>
            <div class="row">
                <?php if(!empty($_GET['funciones'])): ?>
                    <?php foreach($lista->result() as $l): ?>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <a href="<?= site_url('beverage/'.$l->id.'-'.toURL($l->beverage_nombre)) ?>" class="card" id="card-312">
                            <div class="figure">
                                <div class="img" style="background-image:url(<?= base_url('img/beverages/'.$l->miniatura) ?>);"></div>
                                <div class="figCaption"></div>
                                <div class="figView"><span class="icon-eye"></span></div>                            
                            </div>
                            <?php if($l->new==1): ?>
                            <div class="new">
                                <div></div>
                                <span>NEW</span>
                            </div>
                            <?php endif ?>
                            <h2><?= $l->beverage_nombre ?></h2>
                            <div class="cardAddress" style="min-height:50px;">
                                <?= substr(strip_tags($l->descripcion_corta),0,90).'...' ?>
                            </div>
                            <div class="cardAddress" style="text-align:right">
                                <span style="color:red">Leer más</span>
                            </div>
                            <ul class="cardFeat"></ul>
                            <div class="clearfix"></div>
                        </a>
                    </div>
                    <?php endforeach ?>
                <?php else: ?>
                    <?php $gamas = array(); ?>
                    <?php foreach($total->result() as $l): ?>
                        
                        <?php foreach($funciones->result() as $f): ?>
                
                            <?php if($l->{$f->url}==1 && !in_array($f->id,$gamas)): ?>
                                <?php $gamas[] = $f->id; ?>
                                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                    <a href="javascript:change_gama('<?= $f->url ?>')" class="card" id="card-312">
                                        <div class="figure">
                                            <div class="img" style="background-image:url(<?= base_url('img/beverage_funciones/'.$f->icono_lista) ?>);"></div>
                                            <div class="figCaption"></div>
                                            <div class="figView"><span class="icon-eye"></span></div>                            
                                        </div>
                                        <h2><?= $f->beverage_funciones_nombre ?></h2>
                                        <div class="cardAddress" style="min-height:50px;">
                                            Cantidad de productos: <?= count($p[$f->id]) ?>
                                        </div>
                                        <div class="cardAddress" style="text-align:right">
                                            <span style="color:red">Ver productos</span>
                                        </div>
                                        <ul class="cardFeat"></ul>
                                        <div class="clearfix"></div>
                                    </a>
                                </div>
                            <?php endif ?>
                
                        <?php endforeach ?>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
            
                <?php if($lista->num_rows==0): ?>
                    Lo sentimos no hemos encontrado resultados para sus criterios de búsqueda
                <?php endif ?>
            <?php if($lista->num_rows>0 && !empty($_GET['funciones'])): ?>
                <div class="pull-left">
                    <?php $this->load->view('predesign/paginacion',array('rows'=>'12','pages'=>6,'actual'=>$page,'total'=>$total_results,'url'=>'javascript:changepage({{page}})')); ?>
                </div>
                <div class="pull-right search_prop_calc">
                    <?= (($page-1)*12)==0?1:($page-1)*12 ?> - <?= (($page-1)*12)+12 ?> de <?= $total_results ?> productos
                </div>
            <?php endif ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var str = '';
        str+= $("#destselectlabel").html()!='Tipo'?$("#destselectlabel").html()+' /':'';
        str+= $("#gamasselectlabel").html()!='Funciones'?$("#gamasselectlabel").html()+' /':'';
        str+= $("#cselectlabel").html()!='Clasificación'?$("#cselectlabel").html()+' /':'';
        str = str!=''?' de '+str:'';                
        $("#etiquetas").html(str);
    });
    function change_gama(id){        
        var url = '?1<?php
            echo !empty($_GET['descripcion'])?'&descripcion='.$_GET['descripcion']:'';
            echo !empty($_GET['tipos_id'])?'&tipos_id='.$_GET['tipos_id']:'';
            echo !empty($_GET['clasificacion_id'])?'&clasificacion_id='.$_GET['clasificacion_id']:'';
        ?>';
        document.location.href="<?= base_url('beverage/lista') ?>"+url+"&funciones="+id;
    }
</script>
<script>
    $("#filterPropertyForm").hide();
</script>