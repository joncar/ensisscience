<?php $this->load->view('includes/headerMain'); ?>
<div id="wrapper">
    <div id="content" class='max'>        
        <div class="resultsList">
            <h1 class="pull-left">
                Resultados  de <span id="etiquetas"><?= $clase ?></span>
            </h1>
            <div class="clearfix"></div>            
            <?php foreach($lista->result() as $t): ?>
                <?php if(!empty($t->tipos_funciones)): ?>
                    <div class="row">
                        <div class="col-xs-6 col-sm-2">
                            <?= img('img/clases_funciones/'.$t->miniatura) ?>
                        </div>
                        <div class="col-xs-6 col-sm-offset-8 col-sm-2">
                            <?= img('img/clases_funciones/'.$t->logo) ?>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach($t->tipos_funciones as $l): ?>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <a href="<?= base_url('beverage/clases_funciones_read/'.$l->id) ?>" class="card" id="card-312">
                                    <div class="figure">
                                        <div class="img" style="background-image:url(<?= base_url('img/tipos_funciones/'.$l->miniatura) ?>);"></div>
                                        <div class="figCaption"></div>
                                        <div class="figView"><span class="icon-eye"></span></div>                            
                                    </div>                                
                                    <h2><?= $l->tipos_funciones_nombre ?></h2>
                                    <div class="cardAddress" style="min-height:50px;">
                                        <?= substr(strip_tags($l->ficha),0,90).'...' ?>
                                    </div>
                                    <div class="cardAddress" style="text-align:right">
                                        <span style="color:red">Leer más</span>
                                    </div>
                                    <ul class="cardFeat"></ul>
                                    <div class="clearfix"></div>
                                </a>
                            </div>
                        <?php endforeach ?>
                    </div>
                <?php endif ?>
            <?php endforeach ?>            
            <?php if($lista->num_rows==0): ?>
                Lo sentimos no hemos encontrado resultados para sus criterios de búsqueda
            <?php endif ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>