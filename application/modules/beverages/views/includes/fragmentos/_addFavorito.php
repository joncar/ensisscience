<form onsubmit="return addFav()">
    <div class="form-group">
        <label for="exampleInputEmail1">Mi lista</label>
        <a data-target="#addLista" data-toggle="modal" href="javascript:void(0);" id="favBtn">
            Añadir Lista
        </a>
        <?= form_dropdown_from_query('beverages_lista_id', $listas->result, 'id', 'beverages_lista_nombre', 0, 'id="beverages_lista_id"') ?>
    </div>                                        
    <center><button type="submit" class="btn btn-success">Guardar Favorito</button></center>
</form>
<div id="success" class="alert alert-success" style="display:none;">Este producto se añadido a tu lista de favoritos que podrás consultar en el menú lateral</div>
<div id="error" class="alert alert-danger" style="display:none;">Debe seleccionar una lista creada</div>