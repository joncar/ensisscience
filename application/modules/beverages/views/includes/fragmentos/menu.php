<?php 
    $enlace = $this->router->fetch_method()=='index'?'':'beverage';
    $this->load->view('includes/fragmentos/modals'); 
?>
<div class="hidden-xs col-sm-2 col-lg-3" id="innerlogo">
    <a href="<?= site_url($enlace) ?>">
        <?= img('img/logo.png','width:100%') ?>
    </a>
</div>
<div class="col-xs-12 col-sm-11 col-lg-9">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand visible-xs" href="<?= site_url($enlace) ?>"><?= img('img/logo2.png','width:159px') ?></a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">              
          <ul class="nav navbar-nav navbar-right" <?php if($this->router->fetch_method()=='index'): ?>id="menuheaderbar"<?php endif ?>>
            <li><a href="<?= site_url() ?>">Inicio</a></li>
            <li><a href="<?= site_url('beverage/empresa') ?>">Empresa</a></li>
            <li class="dropdown">
                <a href="#funciones" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Funciones <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <?php foreach($this->db->get('beverage_funciones')->result() as $g): ?>
                    <li><a href="<?= base_url('beverage/clases_funciones') ?>/<?= $g->id ?>"><?= $g->beverage_funciones_nombre ?></a></li>
                  <?php endforeach ?>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#clasificaciones" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Clasificaciones <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <?php $id = 0; ?>
                  <?php $this->db->join('tipos','tipos.id = clasificacion.tipos_id'); ?>
                  <?php foreach($this->db->get_where('clasificacion')->result() as $g): ?>
                    <?php if($id!==$g->tipos_id): $id = $g->tipos_id ?>
                    <li class="active"><a href="#"><?= strtoupper($g->tipo_nombre) ?></a></li>
                    <?php endif ?>
                    <li><a href="<?= site_url('beverage/lista') ?>?clasificacion_id=<?= $g->id ?>"><?= $g->clasificacion_nombre ?></a></li>
                  <?php endforeach ?>
                </ul>
            </li>                        
            <li><a href="<?= site_url('beverage/contacto') ?>">Contacto</a></li>            
            <?php if(empty($_SESSION['user'])): ?>            
            <li><a data-target="#signin" data-toggle="modal" href="#" style="color:#5477c8">Mis listas</a></li>
            <?php else: ?>
            <li><a href="<?= site_url('beverage/favoritos') ?>">Mis listas</a></li>
            <li><a href="<?= base_url('main/unlog') ?>" style="color:#5477c8">Salir</a></li>
            <?php endif ?>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
</div>