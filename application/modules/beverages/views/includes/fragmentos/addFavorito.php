<div class="modal fade" id="addFavorito" role="dialog" aria-labelledby="searches-label" aria-hidden="true" style="margin-top:80px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-close"></span></button>
                <h4 class="modal-title" id="searches-label">Añadir a mis favoritos</h4>
            </div>
            <div class="modal-body" id="bodyFavoritos">
                <?= $this->load->view('includes/fragmentos/_addFavorito') ?>
            </div>
        </div>
    </div>
</div>
<script>
    function addFav(){
        if($("#beverages_lista_id").val()!==null){
            $.post('<?= base_url('beverage/addFavoritos') ?>',{beverages_id:<?= $id ?>,beverages_lista_id:$("#beverages_lista_id").val()},function(data){
                $("#success").show();
                $("#error").hide();
            });
        }else{
            $("#success").hide();
            $("#error").show();
        }
        return false;
    }
    
    function refreshLista(){
        $.post('<?= base_url('beverage/refreshFavoritos/'.$id) ?>',{},function(data){
            $("#bodyFavoritos").html(data);
        });
    }
    $("#success").hide();
</script>