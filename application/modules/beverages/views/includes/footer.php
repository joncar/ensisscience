<div class="home-footer">
    <div class="page-wrapper">
        <div class="row">
            <div class="hidden-xs col-sm-6 col-md-3 col-lg-3">
                <ul class="footer-nav pb20">
                    <li class="widget-container recent_properties_sidebar">
                        <h3 class="osLight footer-header">Top Vinos</h3>
                        <div class="propsWidget">
                            <ul class="propList">
                                <?php $x = 0; ?>
                                <?php $this->db->order_by('visitas','DESC'); ?>
                                <?php foreach($this->db->get_where('clasificacion',array('tipos_id'=>1))->result() as $n=>$d): ?>
                                <?php if($x<3 && $d->tipos_id==1): $x++; ?>
                                <li>
                                    <a href="<?= base_url('beverage/lista/') ?>?clasificacion_id=<?= $d->id ?>">
                                        <div class="image">
                                            <?= img('img/clasificaciones/'.$d->imagen_footer,'width:100%'); ?>
                                        </div>
                                        <div class="info text-nowrap">
                                            <div class="name"><?= $d->clasificacion_nombre ?></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <?php endif ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="hidden-xs col-sm-6 col-md-3 col-lg-3">
                <ul class="footer-nav pb20">
                    <li class="widget-container recent_properties_sidebar">
                        <h3 class="osLight footer-header">Top Cervezas</h3>
                        <div class="propsWidget">
                            <ul class="propList">
                                <?php $x = 0; ?>
                                <?php $this->db->order_by('visitas','DESC'); ?>
                                <?php foreach($this->db->get_where('clasificacion',array('tipos_id'=>2))->result() as $n=>$d): ?>                                
                                <?php if($x<3 && $d->tipos_id==2): $x++; ?>
                                <li>
                                    <a href="<?= base_url('beverage/lista/') ?>?clasificacion_id=<?= $d->id ?>">
                                        <div class="image">
                                            <?= img('img/clasificaciones/'.$d->imagen_footer,'width:100%'); ?>
                                        </div>
                                        <div class="info text-nowrap">
                                            <div class="name"><?= $d->clasificacion_nombre ?></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </a>
                                </li>
                                <?php endif ?>
                                <?php endforeach ?>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="text-align:center">
                <ul class="footer-nav pb20">
                    <li class="widget-container widget_recent_entries"> 
                    <a href="http://ensissciences.com/webnew/files/CERTIFICADO%20ENSIS%20SCIENCES%20ISO%2022000%20SAPNISH.pdf">                       
                        <img src="http://ensissciences.com/webnew/img/iso.jpg" style="">
                    </li>
                         
                            <br>
                        <li class="widget-container social_sidebar">
                            <h3 class="osLight footer-header">Síguenos</h3>
                            <ul>

                                <li>
                                    <a href="https://www.facebook.com/pages/Ensis-Sciences/110958968924063" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-facebook"></span>
                                    </a>
                                    <a href="https://twitter.com/rsanzj" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-twitter"></span>
                            
                                    </a> 
                                    <a href="https://www.linkedin.com/in/ramses-sanz-58642130" class="btn btn-sm btn-icon btn-round btn-o btn-white" target="_blank">
                                        <span class="fa fa-linkedin"></span>
                                    </a> 
                                </li>
                            </ul>
                        </li>
                </ul>
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="text-align:center">
                <ul class="footer-nav pb20">
                    <li class="widget-container contact_sidebar">
                        <h3 class="osLight footer-header">Oficinas</h3>
                        <ul>
                            <li class="widget-phone"><span class="fa fa-phone"></span> <?= $this->ajustes->telefono ?></li>                            
                            <li class="widget-address osLight"><p style="line-height:16px;"><?= $this->ajustes->direccion_contacto ?></li>
                            <img src="http://ensissciences.com/webnew/img/Logo_EsadeCreapolis_CMYK1.jpg" style="">
                        </ul>

                </ul>
            </div>
        </div>
               <div class="copyright">© 2016 Ensis Sciences. Spain. All rights reserved</div><br>
    </div>
</div>