<div class="filter">    
    <a href="javascript:void(0);" class="handleFilter"><span class="fa fa-search"></span></a>
    <div class="clearfix"></div>
    <form style="display: block;" class="filterForm" id="filterPropertyForm" role="search" method="get" action="<?= base_url('beverage/lista') ?>">
        <div class="row"><h3>Búsqueda</h3></div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">                    
                        <input class="form-control" name="descripcion" id="descripcion" value="<?= !empty($_GET['descripcion'])?$_GET['descripcion']:'' ?>" placeholder="Introduzca sus parámetros de búsqueda" autocomplete="off" type="text">                    
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="form-group">
                        <label class="hidden-sm hidden-md">Tipos</label>
                        <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                            <span class="dropdown-label" id="destselectlabel">Tipo</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-select">
                            <li <?= empty($_GET['tipos_id'])?'active':'' ?>>
                                <input type="radio" name="tipos_id" value="" <?= empty($_GET['tipos_id'])?'checked':'' ?>><a href="javascript:void(0);">Tipo</a>
                            </li>
                            <?php foreach($this->db->get_where('tipos')->result() as $n=>$d): ?>
                            <li <?= !empty($_GET['tipos_id']) && $_GET['tipos_id']==$d->id?'active':'' ?>>
                                <input type="radio" name="tipos_id" value="<?= $d->id ?>" <?= !empty($_GET['tipos_id']) && $_GET['tipos_id']==$d->id?'checked':'' ?>>
                                <a href="javascript:void(0);"><?= $d->tipo_nombre ?></a>
                            </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>            
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="form-group">
                        <label class="hidden-sm hidden-md">Funciones</label>
                        <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                            <span class="dropdown-label" id="gamasselectlabel">Funciones</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-select">
                            <li <?= empty($_GET['funciones'])?'active':'' ?>>
                                <input type="radio" name="funciones" value="" <?= empty($_GET['funciones'])?'checked':'' ?>><a href="javascript:void(0);">Funciones</a>
                            </li>
                            <?php foreach($this->db->get_where('beverage_funciones')->result() as $n=>$d): ?>
                            <li <?= !empty($_GET['funciones']) && $_GET['funciones']==$d->url?'active':'' ?>>
                                <input type="radio" name="funciones" value="<?= $d->url ?>" <?= !empty($_GET['funciones']) && $_GET['funciones']==$d->url?'checked':'' ?>>
                                <a href="javascript:void(0);"><?= $d->beverage_funciones_nombre ?></a>
                            </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="form-group">
                        <label class="hidden-sm hidden-md">Clasificaciones</label>
                        <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                            <span class="dropdown-label" id="cselectlabel">Clasificación</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-select">
                            <li <?= empty($_GET['clasificacion_id'])?'active':'' ?>>
                                <input type="radio" name="clasificacion_id" value="" <?= empty($_GET['clasificacion_id'])?'checked':'' ?>><a href="javascript:void(0);">Clasificación</a>
                            </li>
                            <?php foreach($this->db->get_where('clasificacion')->result() as $n=>$d): ?>
                            <li <?= !empty($_GET['clasificacion_id']) && $_GET['clasificacion_id']==$d->id?'active':'' ?>>
                                <input type="radio" name="clasificacion_id" value="<?= $d->id ?>" <?= !empty($_GET['clasificacion_id']) && $_GET['clasificacion_id']==$d->id?'checked':'' ?>>
                                <a href="javascript:void(0);"><?= $d->clasificacion_nombre ?></a>
                            </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="form-group">                        
                        <input type='hidden' name='page' id='page' value='<?= empty($_GET['page'])?1:$_GET['page'] ?>'>
                        <input type='hidden' name='order_by' id='order_by' value='<?= empty($_GET['order_by'])?'beverages.id_ASC':$_GET['order_by'] ?>'>
                        <button type="submit" class="btn btn-green mb-10 btn-block" id="filterPropertySubmit">Buscar</button>
                        <!--<a href="javascript:void(0);" class="btn btn-gray display mb-10" id="showAdvancedFilter">Búsqueda avanzada</a>
                        <a href="javascript:void(0);" class="btn btn-gray mb-10" id="hideAdvancedFilter">Cerrar búsqueda avanzada</a>-->
                    </div>
                </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<script>
    function changepage(page){
        $("#page").val(page);
        $("#filterPropertyForm").submit();
    }
    
    $(document).ready(function(){
        $(".order").change(function(){
            $("#order_by").val($(this).val());
            $("#filterPropertyForm").submit();
        });
    });
</script>