<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function tipos($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','en'=>'Inglés'));
            $crud->display_as('tipos_nombre','Nombre');            
            $output = $crud->render();
            $this->loadView($output);
        }  
        
        function clasificacion($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','en'=>'Inglés'));
            $crud->display_as('tipos_nombre','Nombre');
            $crud->set_field_upload('icono','img/clasificaciones');            
            $crud->set_field_upload('imagen','img/clasificaciones');            
            $crud->set_field_upload('imagen_footer','img/clasificaciones');            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function beverage_funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y); 
            $crud->set_subject('Funciones');
            $crud->display_as('beverage_funciones_nombre','Nombre');
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','en'=>'Inglés'));
            $crud->set_field_upload('icono','img/beverage_funciones');            
            $crud->set_field_upload('icono_lista','img/beverage_funciones');            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        function clases_funciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);             
            $crud->set_subject('Clase');                        
            $crud->set_field_upload('miniatura','img/clases_funciones');
            $crud->set_field_upload('logo','img/clases_funciones');
            $output = $crud->render();
            $output->title = 'clases de funciones';
            $this->loadView($output);
        }
        
        function tipos_funciones($x = '',$y = ''){
            if($x=='clonar'){
                if(is_numeric($y)){
                    $propiedad = new Bdsource();
                    $propiedad->where('id',$y);
                    $propiedad->init('tipos_funciones',TRUE);
                    $data = (array)$this->tipos_funciones;
                    $data['miniatura'] = '';
                    $data['foto'] = '';
                    //print_r($data);
                    $propiedad->save($data,'',TRUE);
                    header("Location:".base_url('beverages/admin/tipos_funciones/edit/'.$propiedad->getid()));
                }
            }else{
                $crud = $this->crud_function($x,$y);             
                $crud->set_subject('Tipos de Funciones');                        
                $crud->set_field_upload('miniatura','img/tipos_funciones');            
                $crud->set_field_upload('foto','img/tipos_funciones');            
                $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('beverages/admin/tipos_funciones/clonar').'/');
                $output = $crud->render();
                $output->title = 'Tipos de Funciones';
                $this->loadView($output);
            }
        }
        
        function beverages($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->set_relation_dependency('clasificacion_id','tipos_id','tipos_id');
            $crud->set_relation('clasificacion_id','clasificacion','clasificacion_nombre');
            $crud->set_field_upload('pdf','files/beverages');
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','en'=>'Inglés'))
                     ->field_type('levaduras','checkbox')
                     ->field_type('antioxidantes','checkbox')
                     ->field_type('enzimas','checkbox')
                     ->field_type('activadores','checkbox')
                     ->field_type('nutrientes','checkbox')
                     ->field_type('cortezas_manoproteinas','checkbox')
                     ->field_type('taninos','checkbox')
                     ->field_type('clarificantes','checkbox')
                     ->field_type('bacterias_nutricion','checkbox')
                     ->field_type('potenciador_azucar','checkbox')
                    ->field_type('levaduras_low_cost','checkbox')
                    ->field_type('tratamientos','checkbox')
                    ->field_type('filtracion','checkbox')
                    ->field_type('otros','checkbox')
                    ->field_type('levaduras_segunda_fermentacion','checkbox');
            $crud->display_as('beverage_nombre','Nombre')
                     ->display_as('clasificacion_id','Clasificación')
                     ->display_as('tipos_id','Tipo de bebida')
                     ->display_as('descripcion','Descripción')
                     ->display_as('filtracion','Filtración')
                     ->display_as('filtracion','Filtración');
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('beverages/admin/clonarBeverage').'/');
            $crud->columns('portada','tipos_id','clasificacion_id','beverage_nombre','idioma');
            $crud->set_field_upload('portada','img/beverages');            
            $crud->set_field_upload('miniatura','img/beverages');            
            $output = $crud->render();
            $this->loadView($output);
        }
        
        public function clonarBeverage($id){
            if(is_numeric($id)){
                $propiedad = new Bdsource();
                $propiedad->where('id',$id);
                $propiedad->init('beverages',TRUE);
                $data = (array)$this->beverages;    
                $data['portada'] = '';
                $data['miniatura'] = '';     
                $data['pdf'] = '';
                $propiedad->save($data,'',TRUE);
                header("Location:".base_url('beverages/admin/beverages/edit/'.$propiedad->getid()));
            }
        }
    }
?>
