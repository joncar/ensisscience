<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Front extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function loadView($param = array('view'=>'main')){
            $data = $param;
            if(is_string($data)){
                $data = array('view'=>$data);
            }
            $data['tipo'] = 'foods';
            parent::loadView($data);
        }
        
        function index($x = '',$y = ''){            
            $this->bdsource->init('tipos',TRUE);            
            $clasificacion = new Bdsource('clasificacion');
            $clasificacion->init('clasificacion',FALSE,'',TRUE);            
            $clasificacion1 = new Bdsource('clasificacion');
            $clasificacion1->where('tipos_id',$this->tipos->row(1)->id);
            $clasificacion1->init();            
            $clasificacion2 = new Bdsource('clasificacion');
            $clasificacion2->where('tipos_id',$this->tipos->row(0)->id);
            $clasificacion2->init();
            $funciones = new Bdsource('beverage_funciones');
            $funciones->init();
            
            $this->loadView(array(
                'view'=>'main',                
                'tipos'=>$this->tipos,
                'clasificacion1'=>$clasificacion1,
                'clasificacion2'=>$clasificacion2,
                'clasificacion'=>$clasificacion,
                'funciones'=>$funciones,
                'title'=>'Beverages',                
             ));
        }
        
        function lista(){
            
            if(empty($_GET['page'])){
                $_GET['page'] = 1;
            }            
            $lista = new Bdsource();
            $lista->select = 'beverages.*';
            $limit = ($_GET['page']-1)*12;
            $limit = $limit = 0?1:$limit;
            $lista->limit = array('12',$limit);                        
            $lista->filters = array('tipos_id','clasificacion_id');
            foreach($this->db->get('beverage_funciones')->result() as $f){
                array_push($lista->filters,$f->url);
                if(!empty($_GET['funciones']) && $_GET['funciones'] == $f->url){
                    $_GET[$f->url] = 1;
                }
            }
            if(!empty($_GET['clasificacion_id'])){
                $f = $this->db->get_where('clasificacion',array('id'=>$_GET['clasificacion_id']));
                if($f->num_rows>0){
                    $f = $f->row()->visitas+1;
                    $this->db->update('clasificacion',array('visitas'=>$f),array('id'=>$_GET['clasificacion_id']));
                }
            }
            if(!empty($_GET['descripcion'])){
                $lista->where('(beverages.beverage_nombre like "%'.$_GET['descripcion'].'%" OR MATCH(beverages.beverage_nombre) AGAINST ("'.$_GET['descripcion'].'") OR MATCH(beverages.descripcion) AGAINST ("'.$_GET['descripcion'].'"))',NULL);
            }
            $lista->order_by = array('beverages.orden','asc');            
            $lista->group_by('beverages.id');     
            $lista->where('disponible','1');
            $lista->init('beverages');
            $total_result = clone $lista;            
            $total_result->limit  = array();            
            $total_result->group_by('beverages.id');
            $total_result->filters = array('tipos_id','clasificacion_id');
            $total_result->init('beverages');
            
            $funciones = new Bdsource('beverage_funciones');
            $funciones->init();
            
            $this->loadView(array(
                'view'=>'lista',
                'title'=>'Resultados de búsqueda',
                'lista'=>$lista,
                'page'=>$_GET['page'],
                'funciones'=>$funciones,
                'total'=>$total_result,
                'total_results'=>$total_result->num_rows
             ));
        }
        
        function read($id){
            if(empty($id)){
                throw new Exception('Producto no encontrado',404);
            }
            else{
                $id = explode("-",$id);
                $id = $id[0];
                if(is_numeric($id)){
                    $detail = new Bdsource('beverages');
                    $detail->select = 'beverages.*, tipos.tipo_nombre, tipos.id as tipoid, clasificacion.clasificacion_nombre, clasificacion.id as clasificacionid, clasificacion.imagen';
                    $detail->where('beverages.id',$id);
                    $detail->innerjoin('tipos');
                    $detail->innerjoin('clasificacion');
                    $detail->init();                    

                    $relacionados = new Bdsource('beverages');
                    $relacionados->where('tipos_id',$detail->tipos_id);
                    $relacionados->where('beverages.id != ',$detail->getid());
                    $relacionados->limit = array('4');
                    $relacionados->init();

                    $listas = new Bdsource('beverages_lista');
                    $onfav = new Bdsource('beverages_favoritos');
                    if(!empty($_SESSION['user'])){
                        $listas->where('user_id',$this->user->id); 
                        $listas->init();
                        $onfav->where(array('user_id'=>$this->user->id,'beverages_id'=>$id));
                        $onfav->init();
                    }else{
                        $listas->where('beverages_lista.id',-1);
                        $listas->init();
                        $onfav->where(array('user_id'=>-1));
                        $onfav->init();
                    }                    
                    
                    $onfav = $onfav->num_rows==0?FALSE:TRUE;

                        $this->loadView(array(
                            'view'=>'read',
                            'detail'=>$detail,
                            'title'=>$detail->beverage_nombre,
                            'relacionados'=>$relacionados,
                            'listas'=>$listas,
                            'id'=>$id,
                            'onfav'=>$onfav
                        ));
                }else{
                    throw new Exception('Producto no encontrado',404);
                }
            }
        }
        
        function favoritos(){
            if(empty($_SESSION['user'])){
                header("Location:".base_url('panel')); 
                exit;
            }
            if(empty($_GET['page'])){
                $_GET['page'] = 1;
            }
            
            $lista = new Bdsource();
            $lista->select = 'beverages.*, beverages_favoritos.id as favid';
            $limit = ($_GET['page']-1)*12;
            $limit = $limit = 0?1:$limit;
            $joins = array(               
                array('beverages_favoritos','beverages_favoritos.beverages_id = beverages.id')
            );            
            $lista->limit = array('12',$limit);                        
            $lista->filters = array('beverages_lista_id');
            $lista->group_by('beverages.id');
            $lista->joinNormal($joins);
            $lista->where('beverages_favoritos.user_id',$this->user->id);
            $lista->init('beverages');
            
            $total_result = clone $lista;
            $total_result->limit  = array();
            $total_result->joinNormal($joins);
            $total_result->group_by('beverages.id');
            $total_result->where('beverages_favoritos.user_id',$this->user->id);
            $total_result->init('beverages');
            
            $listas = new Bdsource('beverages_lista');
            $listas->where('user_id',$this->user->id); 
            $listas->init();
            $this->loadView(array(
                'view'=>'favoritos',
                'title'=>'Mis Favoritos',
                'listas'=>$listas,
                'lista'=>$lista,
                'page'=>$_GET['page'],
                'total_results'=>$total_result->num_rows
             ));
        }
        
        function addLista(){            
            $this->form_validation->set_rules('beverages_lista_nombre','Nombre','required');
            if($this->form_validation->run()){                
                    $fav = new Bdsource('beverages_lista');
                    $fav->beverages_lista_nombre = $this->input->post('beverages_lista_nombre');
                    $fav->user_id = $this->user->id;
                    $fav->save(array(),$_POST['id']);
                    $listas = new Bdsource('beverages_lista');
                    $listas->where('user_id',$this->user->id); 
                    $listas->init();
                    echo $this->traduccion->traducir($this->load->view('includes/fragmentos/_addLista',array('listas'=>$listas,'in'=>true),true),$_SESSION['lang']);
            }
        }
        
        function rmLista(){            
            $this->form_validation->set_rules('id','ID','required|greather_than[0]');
            if($this->form_validation->run()){                
                    $fav = new Bdsource('beverages_lista');
                    $fav->remove($_POST['id']);
                    $fav = new Bdsource('beverages_favoritos');
                    $fav->where('beverages_favoritos.beverages_lista_id',$_POST['id']);
                    $fav->remove();
                    $listas = new Bdsource('beverages_lista');
                    $listas->where('user_id',$this->user->id); 
                    $listas->init();
                    echo $this->traduccion->traducir($this->load->view('includes/fragmentos/_addLista',array('listas'=>$listas,'in'=>true),true),$_SESSION['lang']);
            }
        }
        
        function refreshFavoritos($id){            
            $listas = new Bdsource('beverages_lista');
            $listas->where('user_id',$this->user->id); 
            $listas->init();
            echo $this->traduccion->traducir($this->load->view('includes/fragmentos/_addFavorito',array('listas'=>$listas,'id'=>$id),true),$_SESSION['lang']);
        }
        
        function addFavoritos(){
            $this->form_validation->set_rules('beverages_id','Foods ID','required|greather_than[0]');
            $this->form_validation->set_rules('beverages_lista_id','Foods ID','required|greather_than[0]');
            if($this->form_validation->run()){
                $existe = new Bdsource('beverages_favoritos');
                $existe->where(array('beverages_id'=>$_POST['beverages_id'],'user_id'=>$this->user->id,'beverages_lista_id'=>$_POST['beverages_lista_id']));
                $existe->init();
                if($existe->num_rows==0){
                    $fav = new Bdsource('beverages_favoritos');
                    $fav->beverages_id = $this->input->post('beverages_id');
                    $fav->beverages_lista_id = $this->input->post('beverages_lista_id');
                    $fav->user_id = $this->user->id;
                    $fav->save();
                }
            }
        }
        
        function rmFavoritos($id){            
            $_POST['id'] = $id;
            $this->form_validation->set_rules('id','Foods ID','required|greather_than[0]');
            if($this->form_validation->run()){
                $fav = new Bdsource('beverages_favoritos');
                $fav->remove($_POST['id']);
            }
            header("Location:".base_url('beverage/favoritos'));
        }
        
        function contacto(){
            parent::contacto();
        }
        function empresa(){
            parent::empresa();
        }
        
        function clases_funciones($id = ''){
            if(is_numeric($id)){
                $this->db->order_by('orden','asc');
                $lista = $this->db->get_where('clases_funciones');
                $bfunciones = $this->db->get_where('beverage_funciones',array('id'=>$id));
                foreach($lista->result() as $n=>$l){
                    $lista->row($n)->tipos_funciones = array();
                    $this->db->order_by('orden','asc');
                    $funciones = $this->db->get_where('tipos_funciones',array('clases_funciones_id'=>$l->id,'beverage_funciones_id'=>$id));
                    foreach($funciones->result() as $f){
                        $lista->row($n)->tipos_funciones[] = $f;
                    }
                }
                $this->loadView(array('view'=>'clases_funciones','lista'=>$lista,'clase'=>$bfunciones->row()->beverage_funciones_nombre));
            }
        }
        
        function clases_funciones_read($id){            
            if(is_numeric($id)){
                $this->db->select('tipos_funciones.*, beverage_funciones.beverage_funciones_nombre, beverage_funciones.url');
                $this->db->join('beverage_funciones','tipos_funciones.beverage_funciones_id = beverage_funciones.id','left');
                $detail = $this->db->get_where('tipos_funciones',array('tipos_funciones.id'=>$id));
                if($detail->num_rows>0){
                    $this->db->limit('4');
                    if(!empty($detail->row()->beverage_funciones_nombre)){
                        $this->db->where('beverages.'.$detail->row()->url,1);
                    }else{
                        $this->db->where('beverages.id',-1);
                    }
                    $relacionados = $this->db->get_where('beverages');
                    $this->loadView(array('view'=>'clases_funciones_read','detail'=>$detail->row(),'relacionados'=>$relacionados));
                }else{
                    throw new Exception('404, La web que desea consultar no se encuentra disponible','404');
                }
            }else{
                throw new Exception('404, La web que desea consultar no se encuentra disponible','404');
            }
        }
    }
