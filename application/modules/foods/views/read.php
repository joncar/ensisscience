<?= $this->load->view('includes/headerMain') ?>
<div id="wrapper">
    <div id="content" class="max">
    <div class="resultsList col-xs-12 col-sm-12 col-sm-offset-1 col-md-8 col-md-offset-1">
        <div class="hidden-md hidden-sm">
            <?= $this->load->view('includes/searchbox') ?>
        </div>
        <div class="singleTop">
            <div id="carouselFull" class="carousel slide gallery" data-ride="carousel" data-interval="false">
                <ol class="carousel-indicators">
                    <li data-target="#carouselFull" data-slide-to="0"class="active" ></li>
                </ol>
                <div class="carousel-inner">
                    <a href="<?= base_url('img/foods/'.$detail->portada) ?>" data-fancybox-group="slideshow" class="galleryItem item active" style="background-image: url(<?= base_url('img/foods/'.$detail->portada) ?>)"></a>
                </div>
                <a class="left carousel-control" href="#carouselFull" role="button" data-slide="prev"><span class="fa fa-chevron-left"></span></a>
                <a class="right carousel-control" href="#carouselFull" role="button" data-slide="next"><span class="fa fa-chevron-right"></span></a>
            </div>

            <div class="summary">
                <?php if($detail->new==1): ?>
                <div class="new">
                    <div></div>
                    <span>NEW</span>
                </div>
                <?php endif ?>
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <div class="summaryItem">                            
                            <h1 class="pageTitle">
                                    <?= $detail->foods_nombre ?> - <?= $detail->gamas_nombre ?>
                                    <div class="favLink" style="float:none; display:inline-block;">
                                    <?php if(!empty($_SESSION['user'])): ?>                                        
                                        <a class="<?= $onfav?'noSigned':'Signed' ?>" data-target="#addFavorito" data-toggle="modal" href="javascript:void(0);" id="favBtn">
                                            <span class="fa fa-heart-o" style="font-size: 28px;"></span>
                                        </a>
                                    <?php else: ?>
                                        <a href="#" data-toggle="modal" data-target="#signin" class="Signed">
                                            <span class="fa fa-heart-o" style="font-size: 28px;"></span>
                                        </a>
                                    <?php endif ?>
                                    </div>
                            </h1>
                            <div class="clearfix"></div>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

        <div class="description" style="border-bottom: 1px solid black;">
            <div class="entry-content">
                <?= $detail->descripcion ?>
                <div class="clearfix"></div>
            </div>
        </div>       

        <div class="amenities" style="border:0px">
            <h3>Aplicaciones</h3>
            <div class="row" style="margin-left:0px; margin-right:0px;">
                <?php foreach($aplicaciones->result() as $d): ?>
                <div class="iconsapp">
                    <a href="<?= base_url('food/lista/') ?>?aplicaciones_id=<?= $d->id ?>">
                        <?= img('img/aplicaciones/'.$d->icono,'width:60px; height:auto;'); ?><br/><br/><div style='color:black'><?= $d->aplicaciones_nombre ?></div>
                    </a>
                 </div>
                <?php endforeach ?>
            </div>
        </div>        
        <div class="amenities">
            <h3>Destinatarios</h3>
            <div class="row" style="margin-left:0px; margin-right:10px;">
                <?php foreach($destinatarios->result() as $d): ?>
                <div class="iconsapp iconsapplicaciones">
                    <a href="<?= base_url('food/lista/') ?>?destinatarios_id=<?= $d->id ?>" style="text-decoration: none;">
                        <span class="<?= $d->icono ?> s-icon" style="position:static; font-size:60px; line-height:50px;"></span><br/><br/>
                        <span style="text-align:center; color:black;"><?= $d->destinatarios_nombre ?></span>
                    </a>
                 </div>
                <?php endforeach ?>
            </div>
        </div>
        <?php if(!empty($detail->pdf)): ?>
        <div class="amenities">
            <h3>Descargar PDF</h3>
            <div class="row" style="margin-left:0px; margin-right:10px;">                
                <div class="iconsapp">
                    <a href="<?= base_url('files/foods/'.$detail->pdf) ?>" target="_blank">
                        <?= img('img/pdf.png','width:60px; height:auto;'); ?><br/><br/><div style='color:black'></div>
                    </a>
                 </div>
            </div>
        </div>
        <?php endif ?>
        <div class="amenities" style="border:0px">
            <h3>Compartir</h3>
            <div class="share" style="border:0px;">
                <div class="row" style="margin-left:0px; margin-right:10px;">
                        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= site_url('food/'.$detail->getid().'-'.toURL($detail->foods_nombre)) ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                                    return false;"
                           target="_blank" title="Share on Facebook" class="btn btn-sm btn-o btn-facebook">
                            <span class="fa fa-facebook"></span> Facebook
                        </a>
                        <a href="https://twitter.com/share?url=<?= site_url('food/'.$detail->getid().'-'.toURL($detail->foods_nombre)) ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                                    return false;"
                           target="_blank" title="Share on Twitter" class="btn btn-sm btn-o btn-twitter">
                            <span class="fa fa-twitter"></span> Twitter
                        </a>
                        <a href="https://plus.google.com/share?url=<?= site_url('food/'.$detail->getid().'-'.toURL($detail->foods_nombre)) ?>"
                           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');
                                    return false;"
                           target="_blank" title="Share on Google+" class="btn btn-sm btn-o btn-google">
                            <span class="fa fa-google-plus"></span> Google
                        </a>
                        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?= site_url('food/'.$detail->getid().'-'.toURL($detail->foods_nombre)) ?>&title=<?= $detail->foods_nombre ?>" 
                           title="Share on LinkedIn" class="btn btn-sm btn-o btn-linkedin" target="_blank" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=350,width=480');
                                    return false;">
                            <span class="fa fa-linkedin"></span> LinkedIn
                        </a>
                 </div>
            </div>
        </div>
    </div>    
    <div class="resultsList rel col-xs-12 col-sm-12 col-md-3" style="margin-top: 40px;">
        <h4>Productos Relacionados</h4>
        <?php foreach($relacionados->result() as $l): ?>
            <div class="col-xs-12 col-sm-4 col-md-12">
                <a href="<?= site_url('food/'.$l->id.'-'.toURL($l->foods_nombre)) ?>" class="card" id="card-312">
                     <?php if($l->new==1): ?>
                    <div class="new">
                        <div></div>
                        <span>NEW</span>
                    </div>
                    <?php endif ?>
                    <div class="figure">
                        <div class="img" style="background-image:url(<?= base_url('img/foods/'.$l->miniatura) ?>);"></div>
                        <div class="figCaption"></div>
                        <div class="figView"><span class="icon-eye"></span></div>                            
                    </div>
                    <h2><?= $l->foods_nombre ?></h2>
                    <div class="cardAddress">
                        
                    </div>
                    <div class="cardAddress" style="text-align:right">
                        <span style="color:red">Leer más</span>
                    </div>
                    <ul class="cardFeat"></ul>
                    <div class="clearfix"></div>
                </a>
            </div>
            <?php endforeach ?>
    </div>
    </div>
</div>
<?php $this->load->view('includes/fragmentos/addFavorito'); ?>
<?php $this->load->view('includes/fragmentos/addLista'); ?>
<script>
    $("#filterPropertyForm").hide();
</script>