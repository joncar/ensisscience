<div class="filter">    
    <a href="javascript:void(0);" class="handleFilter"><span class="fa fa-search"></span></a>
    <div class="clearfix"></div>
    <form style="display: block;" class="filterForm" id="filterPropertyForm" role="search" method="get" action="<?= base_url('food/lista') ?>">
        <div class="row"><h3>Búsqueda</h3></div>
        <div class="row">
            <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                <div class="form-group">                    
                        <input class="form-control" name="descripcion" id="descripcion" value="<?= !empty($_GET['descripcion'])?$_GET['descripcion']:'' ?>" placeholder="Introduzca sus parámetros de búsqueda" autocomplete="off" type="text">                    
                </div>
            </div>
            <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="form-group">
                        <label class="hidden-sm hidden-md">Destinado a</label>
                        <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                            <span class="dropdown-label" id="destselectlabel">Destinado a</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-select">
                            <li <?= empty($_GET['destinatarios_id'])?'active':'' ?>>
                                <input type="radio" name="destinatarios_id" value="" <?= empty($_GET['destinatarios_id'])?'checked':'' ?>><a href="javascript:void(0);">Destinado a</a>
                            </li>
                            <?php foreach($this->db->get_where('destinatarios',array('idioma'=>$_SESSION['lang']))->result() as $n=>$d): ?>
                            <li <?= !empty($_GET['destinatarios_id']) && $_GET['destinatarios_id']==$d->id?'active':'' ?>>
                                <input type="radio" name="destinatarios_id" value="<?= $d->id ?>" <?= !empty($_GET['destinatarios_id']) && $_GET['destinatarios_id']==$d->id?'checked':'' ?>>
                                <a href="javascript:void(0);"><?= $d->destinatarios_nombre ?></a>
                            </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="form-group">
                        <label class="hidden-sm hidden-md">Gamas</label>
                        <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                            <span class="dropdown-label" id="gamasselectlabel">Gamas</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-select">
                            <li <?= empty($_GET['gamas_id'])?'active':'' ?>>
                                <input type="radio" name="gamas_id" value="" <?= empty($_GET['gamas_id'])?'checked':'' ?>><a href="javascript:void(0);">Gamas</a>
                            </li>
                            <?php foreach($this->db->get_where('gamas',array('idioma'=>$_SESSION['lang']))->result() as $n=>$d): ?>
                            <li <?= !empty($_GET['gamas_id']) && $_GET['gamas_id']==$d->id?'active':'' ?>>
                                <input type="radio" name="gamas_id" value="<?= $d->id ?>" <?= !empty($_GET['gamas_id']) && $_GET['gamas_id']==$d->id?'checked':'' ?>>
                                <a href="javascript:void(0);"><?= $d->gamas_nombre ?></a>
                            </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                    <div class="form-group">
                        <label class="hidden-sm hidden-md">Aplicaciones</label>
                        <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                            <span class="dropdown-label" id="aplselectlabel">Aplicaciones</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-select">
                            <li <?= empty($_GET['aplicaciones_id'])?'active':'' ?>>
                                <input type="radio" name="aplicaciones_id" value="" <?= empty($_GET['aplicaciones_id'])?'checked':'' ?>><a href="javascript:void(0);">Aplicaciones</a>
                            </li>
                            <?php foreach($this->db->get_where('aplicaciones',array('idioma'=>$_SESSION['lang']))->result() as $n=>$d): ?>
                            <li <?= !empty($_GET['aplicaciones_id']) && $_GET['aplicaciones_id']==$d->id?'active':'' ?>>
                                <input type="radio" name="aplicaciones_id" value="<?= $d->id ?>" <?= !empty($_GET['aplicaciones_id']) && $_GET['aplicaciones_id']==$d->id?'checked':'' ?>>
                                <a href="javascript:void(0);"><?= $d->aplicaciones_nombre ?></a>
                            </li>
                            <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
                    <div class="form-group">                        
                        <input type='hidden' name='page' id='page' value='<?= empty($_GET['page'])?1:$_GET['page'] ?>'>
                        <input type='hidden' name='order_by' id='order_by' value='<?= empty($_GET['order_by'])?'foods.id_ASC':$_GET['order_by'] ?>'>
                        <button type="submit" class="btn btn-green mb-10 btn-block" id="filterPropertySubmit">Buscar</button>
                        <!--<a href="javascript:void(0);" class="btn btn-gray display mb-10" id="showAdvancedFilter">Búsqueda avanzada</a>
                        <a href="javascript:void(0);" class="btn btn-gray mb-10" id="hideAdvancedFilter">Cerrar búsqueda avanzada</a>-->
                    </div>
                </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<script>
    function changepage(page){
        $("#page").val(page);
        $("#filterPropertyForm").submit();
    }
    
    $(document).ready(function(){
        $(".order").change(function(){
            $("#order_by").val($(this).val());
            $("#filterPropertyForm").submit();
        });
    });
</script>