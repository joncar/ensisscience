<?php 
    $enlace = $this->router->fetch_method()=='index'?'':'food';
    $this->load->view('includes/fragmentos/modals'); 
?>
<div class="hidden-xs col-sm-2 col-lg-3" id="innerlogo">
    <a href="<?= site_url($enlace) ?>">
        <?= img('img/logo.png','width:100%') ?>
    </a>
</div>
<div class="col-xs-12 col-sm-12 col-lg-9">
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand visible-xs" href="<?= site_url($enlace) ?>"><?= img('img/logo2.png','width:159px') ?></a>
        </div>        
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">              
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<?= site_url() ?>">Inicio</a></li>            
            <li><a href="<?= site_url('food/empresa') ?>">Empresa</a></li>            
            <li class="dropdown">
                <a href="#gamas" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Gamas <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <?php foreach($this->db->get_where('gamas',array('idioma'=>$_SESSION['lang']))->result() as $g): ?>
                    <li><a href="<?= site_url('food/lista') ?>?gamas_id=<?= $g->id ?>"><?= $g->gamas_nombre ?></a></li>
                  <?php endforeach ?>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#aplicaciones" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Aplicaciones <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <?php foreach($this->db->get_where('aplicaciones',array('idioma'=>$_SESSION['lang']))->result() as $g): ?>
                    <li><a href="<?= site_url('food/lista') ?>?aplicaciones_id=<?= $g->id ?>"><?= $g->aplicaciones_nombre ?></a></li>
                  <?php endforeach ?>
                </ul>
            </li>
            <li class="dropdown">
                <a href="#destinatarios" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Destinado a <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <?php foreach($this->db->get_where('destinatarios',array('idioma'=>$_SESSION['lang']))->result() as $g): ?>
                    <li><a href="<?= site_url('food/lista') ?>?destinatarios_id=<?= $g->id ?>"><?= $g->destinatarios_nombre ?></a></li>
                  <?php endforeach ?>
                </ul>
            </li>                        
            <li><a href="<?= site_url('food/contacto') ?>">Contacto</a></li>
            <?php if(empty($_SESSION['user'])): ?>            
            <li><a data-target="#signin" data-toggle="modal" href="#" style="color:#ef6629">Mis listas</a></li>
            <?php else: ?>
            <li><a href="<?= site_url('food/favoritos') ?>">Mis listas</a></li>
            <li><a href="<?= base_url('main/unlog') ?>" style="color:#ef6629">Salir</a></li>
            <?php endif ?>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Idioma <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="<?= site_url('main/traducir/es') ?>">Español</a></li>
                    <li><a href="<?= site_url('main/traducir/en') ?>">Ingles</a></li>
                </ul>
            </li>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
</div>