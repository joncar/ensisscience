<div class="modal fade" id="addLista" role="dialog" aria-labelledby="searches-label" aria-hidden="true" style="margin-top:80px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-close"></span></button>
                <h4 class="modal-title" id="searches-label">Administrar mis listas</h4>
            </div>
            <div class="modal-body" id="addListaBody">
                <?= $this->load->view('includes/fragmentos/_addLista') ?>
            </div>
        </div>
    </div>
</div>
<script>
    function addLis(){
        var id = $("#id").val();
        $.post('<?= base_url('food/addLista') ?>',{foods_lista_nombre:$("#nombre").val(),id:id},function(data){
            $("#addListaBody").html(data);
            if(typeof(refreshLista)!==undefined){
                refreshLista();
            }
        });
        return false;
    }
    $("#success").hide();
    
    function editLista(id,nombre){
        $("#id").val(id);
        $("#nombre").val(nombre);
    }
    
    function removeLista(id){
        $.post('<?= base_url('food/rmLista') ?>',{id:id},function(data){
            $("#addListaBody").html(data);
            if(typeof(refreshLista)!==undefined){
                refreshLista();
            }
        });
    }
</script>