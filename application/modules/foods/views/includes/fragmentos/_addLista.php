    <form onsubmit="return addLis()">
        <table class="table table-bordered">
            <thead>
                <tr><th colspan="2">Nombre</th></tr>

            </thead>
            <tbody>
                <?php foreach ($listas->result() as $t): ?>
                    <tr>
                        <td><?= $t->foods_lista_nombre ?></td>
                        <td>
                            <a href="javascript:editLista('<?= $t->id ?>','<?= $t->foods_lista_nombre ?>')"><i class="fa fa-edit"></i></a> 
                            <a href="javascript:removeLista('<?= $t->id ?>')"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                <?php endforeach ?>
                <tr><td colspan="2"><input type="text" id="nombre" name="foods_lista_nombre" placeholder="Introduce el nombre de la lista y pulsa enter" class="form-control"><input type="hidden" name="id" id="id" value=""></td></tr>
            </tbody>
        </table>                
    </form>
