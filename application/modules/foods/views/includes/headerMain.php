<div id="header" class="row" style="margin-left:0px; margin-right:0px;">
    <div class="hidden-xs col-lg-1 logo">
        <a href="<?= site_url('food') ?>">
            <span class="navIcon flaticondos-home166 marker"></span>
            <span class="logoText"></span>
        </a>
    </div>
    <div class="col-lg-11">
        <?= $this->load->view('includes/fragmentos/menu') ?>
    </div>
</div>

<div id="leftSide">
    <nav class="leftNav scrollable">
        <div class="menu-leftside-menu-container">
            <ul class="menu">
                <?php if(!empty($_SESSION['user'])): ?>
                    <li class="icon-heart">
                        <a href="<?= base_url('food/favoritos') ?>">
                            <span class="navIcon"></span>
                            <span class="navLabel">Favoritos</span>
                        </a>
                    </li>
                <?php else: ?>
                    <li class="icon-heart">                        
                       <a href="#" data-toggle="modal" data-target="#signin">
                            <span class="navIcon"></span>
                            <span class="navLabel">Favoritos</span>
                        </a>
                    </li>
                <?php endif ?>
                    <li class="">
                        <a href="<?= base_url('beverage') ?>">
                            <span class="navIcon flaticon-alcohol149"></span>
                            <span class="navLabel">Beverages</span>
                        </a>
                    </li>
                    <li class="fa fa-ellipsis-h">
                        <a href="javascript:void(0);" class='navHandler'>
                            <span class="navIcon" style="color:white; font-size:22px;"></span>
                            <span class="navLabel" id="">Abrir</span>
                        </a>
                    </li>
            </ul>
        </div>    
    </nav>
</div>