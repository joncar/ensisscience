<div id="hero-container">
    <div id="slideshow">
        <div style='background-image: url(<?= base_url('img/uploads/bg-1.jpg') ?>)'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-2.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-3.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-4.jpg') ?>); display:none;'></div>
        <div style='background-image: url(<?= base_url('img/uploads/bg-5.jpg') ?>); display:none;'></div> 
        <div style='background-image: url(<?= base_url('img/uploads/bg-6.jpg') ?>); display:none;'></div>           
    </div>
    <div class="slideshowShadow"></div>
    <div class="home-header menuontop" id='menu'>
        <div class="col-xs-12" id="homelogo2" style="text-align:center;">
            <a href="<?= site_url() ?>">
                <?= img('img/logo.png','width:40%') ?>
            </a>
        </div>
        <div class="col-lg-3 hidden-xs home-logo osLight" id="homelogo">
            <a href="<?= site_url() ?>">
                <?= img('img/logo.png') ?>
            </a>
        </div>
        
        <?php $this->load->view('includes/fragmentos/menu'); ?>
    </div>
    <div class="home-caption">
        <h1 class="home-title">INGREDIENTES FUNCIONALES PARA LA ALIMENTACIÓN</h1>
        <div class="home-subtitle"></div>
    </div>
    <div class="search-panel">
        <form class="form-inline" method="get" action="<?= base_url('food/lista') ?>">            
            <div class="form-group">
                <input type="search" size='50' class="form-control" id="descripcion" name="descripcion" placeholder="Introduce las palabras separadas por coma" autocomplete="off">
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Destinado a</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="destinatarios_id" value="" checked="checked"><a href="javascript:void(0);">Destinado a</a>
                    </li>
                    <?php foreach($dest->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="destinatarios_id" value="<?= $d->id ?>"><a href="javascript:void(0);"><?= $d->destinatarios_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Gama</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="gamas_id" value="" checked="checked"><a href="javascript:void(0);">Gama</a>
                    </li>
                    <?php foreach($gamas->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="gamas_id" value="<?= $d->id ?>"><a href="javascript:void(0);"><?= $d->gamas_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <div class="form-group hidden-xs adv">
                <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-white dropdown-toggle">
                    <span class="dropdown-label">Aplicaciones</span>&nbsp;&nbsp;&nbsp;<span class="caret"></span>
                </a>
                <ul class="dropdown-menu dropdown-select">
                    <li class="active">
                        <input type="radio" name="aplicaciones_id" value="" checked="checked"><a href="javascript:void(0);">Aplicaciones</a>
                    </li>
                    <?php foreach($aplicaciones->result() as $n=>$d): ?>
                    <li>
                        <input type="radio" name="aplicaciones_id" value="<?= $d->id ?>"><a href="javascript:void(0);"><?= $d->aplicaciones_nombre ?></a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>            
            <div class="form-group">
                <input type="submit" id="searchPropertySubmit" class="btn btn-green" value="Buscar">
                <a href="javascript:void(0);" class="btn btn-o btn-white pull-right visible-xs" id="advanced">Búsqueda Avanzada <span class="fa fa-angle-up"></span></a>
            </div>
        </form>
    </div>
</div>
<div class="spotlight" id="page" >
    <div class="s-title osLight">Busque su ingrediente perfecto para su industria</div>
    <div class="s-text osLight">Si no encuentra su ingrediente nosotros le asesoraremos con nuestra amplia experiéncia en el sector</div>
</div>
<div class="page-wrapper no-touch">
    <div class="page-content">

        <div id="post-2" class="post-2 page type-page status-publish hentry">
            <div class="entry-content">
                <h2 id="destinatarios" class="osLight centered">Destinado a</h2>
                <div class="row pb40">
                    <?php foreach($dest->result() as $d): ?>
                            <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3 s-menu-item">
                                <a href="<?= base_url('food/lista/') ?>?destinatarios_id=<?= $d->id ?>">
                                    <span class="<?= $d->icono_main ?> s-icon"></span>
                                    <div class="s-content">
                                        <h2 class="centered s-main osLight"><?= $d->destinatarios_nombre ?></h2>
                                        <h3 class="s-sub osLight"><?= $d->resumen ?></h3>
                                    </div>
                                </a>
                            </div>                    
                    <?php endforeach ?>
                </div>
                <h2 id="gamas" class="centered osLight">Gamas</h2>
                <div class="row pb40">
                    <?php foreach($gamas->result() as $d): ?>
                    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                        <a href="<?= base_url('food/lista/') ?>?gamas_id=<?= $d->id ?>" class="propWidget-2">
                            <div class="fig">
                                <?= img('img/gamas/'.$d->icono,'',TRUE,'class="scale" alt="" data-scale="best-fill" data-align="center"') ?>
                                <?= img('img/gamas/'.$d->icono,'',TRUE,'class="blur scale" alt="" data-scale="best-fill" data-align="center"') ?>
                                <div class="opac"></div>                                
                                <h3 class="osLight"><?= $d->gamas_nombre ?></h3>
                                <div class="address"></div>
                            </div>
                        </a>
                    </div>
                    <?php endforeach ?>
                </div>                
                <h2 id="aplicaciones" class="centered osLight">Aplicaciones</h2>
                <div class="row pb40">                    
                        <?php for($i=0;$i<4;$i++): if($i<$aplicaciones->num_rows): $d = $aplicaciones->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('food/lista/') ?>?aplicaciones_id=<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/aplicaciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->aplicaciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                    </div>
                <div class="row pb40">
                    <?php for($i=4;$i<8;$i++): if($i<$aplicaciones->num_rows): $d = $aplicaciones->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('food/lista/') ?>?aplicaciones_id=<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/aplicaciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->aplicaciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                </div>
                <div class="row pb40">
                    <?php for($i=8;$i<12;$i++): if($i<$aplicaciones->num_rows): $d = $aplicaciones->row($i); ?>                    
                        <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                        <div class="agent">
                            <a href="<?= base_url('food/lista/') ?>?aplicaciones_id=<?= $d->id ?>"  class="agent-avatar">
                                <?= img('img/aplicaciones/'.$d->icono,'width:150px'); ?>
                                <div class="ring"></div>
                            </a>
                            <div class="agent-name osLight"><?= $d->aplicaciones_nombre ?></div>                            
                        </div>
                    </div>
                    <?php endif; endfor ?>
                </div>                                
                <div class="clearfix"></div>
            </div>
        </div>

    </div>
</div>
<div class="row contactenos" style="margin-left:0px; margin-right:0px;">
    <h2 class="centered osLight">Contáctenos</h2>
    <p align='center'>Si tiene alguna duda o quiere preguntar por un producto</p>
    <div class='col-xs-12 col-sm-6 col-sm-offset-3'>
        <form id="formcontact" onsubmit="return sendMessage()" class="contactPageForm">            
            <div class="row">
                <div id="cp_response" class="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="cp_name">Nombre <span class="text-red">*</span></label>
                        <input id="nombre" name="nombre" placeholder="Escriba su nombre" class="form-control" type="text" required="">
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="cp_email">Email <span class="text-red">*</span></label>
                        <input id="email" name="email" placeholder="Escriba su Email" class="form-control" type="email" required="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <label for="cp_message">Mensaje <span class="text-red">*</span></label>
                        <textarea id="mensaje" name="mensaje" placeholder="Escriba su mensaje" rows="3" class="form-control" required=""></textarea>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="form-group">
                        <button class="btn btn-green">Enviar mensaje</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<section class="facts uk-section uk-section-large uk-light">
        <div class="pattern" style="position:absolute; top:0px;"></div>
        <div class="container" style="position:relative; z-index:1000">            
            <div data-uk-scrollspy="target: &gt; .tw-heading; cls:uk-animation-slide-bottom-small; delay: 300">
                <div class="tw-element tw-heading uk-text-center uk-scrollspy-inview uk-animation-slide-bottom-small" style="">                    
                    <button class="uk-margin-top tw-video-icon" data-target="#modalvideo" data-toggle="modal" style="background-color: rgb(132, 211, 212); padding-top: 9px; padding-left: 15px;">
                        <i style="color: rgb(255, 255, 255); font-size: 38px;" class="fa fa-play"></i>
                    </button>
                    <!-- This is the modal with the default close button -->
                </div>
            </div>                
        </div>
    </section>
<div class='page-wrapper'>
    <h2 class="centered osLight">Nuestros Productos</h2>
    <div class="row pb40">
        <div id="home-testimonials" class="carousel slide carousel-wb mb20" data-ride="carousel">
            <ol class="carousel-indicators">
                <?php $opiniones = $this->db->get_where('opiniones',array('tipo'=>2,'idioma'=>$_SESSION['lang'])); ?>
                <?php foreach($opiniones->result() as $n=>$v): ?>
                    <li data-target="#home-testimonials" data-slide-to="<?= $n ?>" <?php if($n==0): ?>class="active"<?php endif ?> ></li>
                <?php endforeach ?>                
            </ol>
            <div class="carousel-inner">
                <?php foreach($opiniones->result() as $n=>$v): ?>
                    <div class="item <?php if($n==0): ?>active<?php endif ?>">
                        <img src="<?= base_url('img/opiniones/'.$v->foto) ?>" class="home-testim-avatar" alt="<?= $v->nombre ?>">
                        <div class="home-testim">
                            <div class="home-testim-text">
                                <?= $v->comentario ?>
                            </div>
                            <div class="home-testim-name"><?= $v->nombre ?></div>
                        </div>                        
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<div class='page-wrapper' style="padding:40px 0 0px; margin:0 ; width:100%;">
    <?php $coords = maptoarray($this->ajustes->mapa_contacto); $cord = $coords[0].','.$coords[1]; ?>    
    <div id='mapa' style="width:100%; height:300px;"></div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyB_R8j4rq6MXoV17xrbyyh2rwZS6pTMd9w&libraries=drawing,geometry,places"></script> 
    <script>
        var mapaContent = document.getElementById('mapa');
        var center = new google.maps.LatLng(<?= $cord ?>);    
        var mapOptions = {
                    zoom: 14,
                    center: center,               
        };
        var map = new google.maps.Map(mapaContent, mapOptions);
        new google.maps.Marker({position: new google.maps.LatLng(<?= $cord ?>),map:map});
    </script>
</div>
<div class="modal fade" id="modalvideo" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
          <div class="embed-responsive embed-responsive-16by9">
            <iframe id="youtubeModalVideo" class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/gbiaKJjs4e8" frameborder="0" allowfullscreen></iframe>
          </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?= $this->load->view('includes/footer') ?>
<!---- Mostrar Video --->
<script type='text/javascript'>function setCookie(name,value,expires,path,domain,secure){document.cookie=name+"="+escape(value)+((expires==null)?"":"; expires="+expires.toGMTString())+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+((secure==null)?"":"; secure")}function getCookie(name){var cname=name+"=";var dc=document.cookie;if(dc.length>0){begin=dc.indexOf(cname);if(begin!=-1){begin+=cname.length;end=dc.indexOf(";",begin);if(end==-1)end=dc.length;return unescape(dc.substring(begin,end))}}return null}function delCookie(name,path,domain){if(getCookie(name)){document.cookie=name+"="+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+"; expires=Thu, 01-Jan-70 00:00:01 GMT"}}</script>
<!-- Gestión barra aviso cookies -->
<script type='text/javascript'>
    function mostrarVideo(){
        var comprobar = getCookie("videointro");
        console.log(comprobar);
        if (comprobar != null) {}
        else {
        var expiration = new Date();
        expiration.setTime(expiration.getTime() + (60000*60*24*365));
        setCookie("videointro","1",expiration);
        $("#modalvideo").modal('toggle');
        }
    }
    function clickVideo(){
        $("#modalvideo").modal('toggle');
    }
</script>
<!--- /Video --->
<script>
    $(document).ready(function(){
        mostrarVideo();
        $("#modalvideo").on('hide.bs.modal',function(){
            $("#youtubeModalVideo").attr('src','');
        });
        $("#modalvideo").on('show.bs.modal',function(){
            $("#youtubeModalVideo").attr('src','https://www.youtube.com/embed/gbiaKJjs4e8');
        });
        $(window).on('scroll',function(){
            if($(this).scrollTop()>50)
                $("#menu").addClass('menuonbottom').removeClass('menuontop');
            else
                $("#menu").addClass('menuontop').removeClass('menuonbottom');
        });
        
        $("a[href*=#]").click(function(e){
            e.preventDefault();
            var target = $(this).attr('href');
            target = target.split("#");
            target = $("#"+target[1]);
            
            $('html, body').stop().animate({'scrollTop': parseInt(target.offset().top)-30}, 900, 'swing');        
        });
    });
    
    function sendMessage(){
        if($("#nombre").val()!=='' && $("#email").val()!=='' && $("#mensaje").val()!==''){
            var datos = document.getElementById('formcontact');
            var f = new FormData(datos);
            console.log(f);
            $.ajax({
                url:'<?= base_url('main/contacto') ?>',
                data:f,
                cache: false,
                contentType: false,
                processData: false,
                type:'POST',
                success:function(data){
                    alert('Su mensaje ha sido enviado con éxito');
                }
            });
        }else{
            alert('por favor complete los datos, para poder contactarlo.');
        }
        return false;
    }
</script>
