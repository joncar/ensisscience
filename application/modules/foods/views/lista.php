<?php 
    $p = array();
    $gama = '';
    foreach($total->result() as $t){
        if($gama!=$t->gamas_id){
            $gama = $t->gamas_id;            
        }
        $p[$gama][] = $t->gamas_id;
    }
?>
<?php $this->load->view('includes/headerMain'); ?>
<div id="wrapper" style="overflow: auto;">
    <div id="content" class='max'>
        <?= $this->load->view('includes/searchbox') ?>
        <div class="resultsList">
            <h1 class="pull-left">
                Resultados <span id="etiquetas"></span>
            </h1>
            <div class="clearfix"></div>
            <div class="row">
                <?php if(!empty($_GET['gamas_id'])): ?>
                    <?php foreach($lista->result() as $l): ?>
                    <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                        <a href="<?= site_url('food/'.$l->id.'-'.toURL($l->foods_nombre)) ?>" class="card">
                            <div class="figure">
                                <div class="img" style="background-image:url(<?= base_url('img/foods/'.$l->miniatura) ?>);"></div>
                                <div class="figCaption"></div>
                                <div class="figView"><span class="icon-eye"></span></div>                            
                            </div>
                            <?php if($l->new==1): ?>
                            <div class="new">
                                <div></div>
                                <span>NEW</span>
                            </div>
                            <?php endif ?>
                            <h2><?= $l->foods_nombre ?></h2>
                            <div class="cardAddress" style="min-height:50px;"></div>
                            <div class="cardAddress" style="text-align:right">
                                <span style="color:red">Leer más</span>
                            </div>
                            <ul class="cardFeat"></ul>
                            <div class="clearfix"></div>
                        </a>
                    </div>
                    <?php endforeach ?>
                <?php else: ?>
                    <?php $gamas = array(); ?>
                    <?php foreach($total->result() as $l): ?>                        
                        <?php if(!in_array($l->gamas_id,$gamas)): ?>
                            <?php $gamas[] = $l->gamas_id; ?>
                            <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
                                <a href="javascript:change_gama('<?= $l->gamas_id ?>')" class="card" id="card-312">
                                    <div class="figure">
                                        <div class="img" style="background-image:url(<?= base_url('img/gamas/'.$l->gamafoto) ?>);"></div>
                                        <div class="figCaption"></div>
                                        <div class="figView"><span class="icon-eye"></span></div>                            
                                    </div>
                                    <h2><?= $l->gamas_nombre ?></h2>
                                    <div class="cardAddress" style="min-height:50px;">
                                        Cantidad de productos: <?= count($p[$l->gamas_id]) ?>
                                    </div>
                                    <div class="cardAddress" style="text-align:right">
                                        <span style="color:red">Ver productos</span>
                                    </div>
                                    <ul class="cardFeat"></ul>
                                    <div class="clearfix"></div>
                                </a>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php endif ?>
            </div>
            
                <?php if($lista->num_rows==0): ?>
                    Lo sentimos no hemos encontrado resultados para sus criterios de búsqueda
                <?php endif ?>
            <?php if($lista->num_rows>0 && !empty($_GET['gamas_id'])): ?>
                <div class="pull-left">
                    <?php $this->load->view('predesign/paginacion',array('rows'=>'12','pages'=>6,'actual'=>$page,'total'=>$total_results,'url'=>'javascript:changepage({{page}})')); ?>
                </div>
                <div class="pull-right search_prop_calc">
                    <?= (($page-1)*12)==0?1:($page-1)*12 ?> - <?= (($page-1)*12)+12 ?> de <?= $total_results ?> productos
                </div>
            <?php endif ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var str = '';
        str+= $("#destselectlabel").html()!='Destinado a'?$("#destselectlabel").html()+' /':'';
        str+= $("#gamasselectlabel").html()!='Gamas'?$("#gamasselectlabel").html()+' /':'';
        str+= $("#aplselectlabel").html()!='Aplicaciones'?$("#aplselectlabel").html()+' /':'';
        str = str!=''?' de '+str:'';
        $("#etiquetas").html(str);
    });
    
    function change_gama(id){        
        var url = '?1<?php
            echo !empty($_GET['descripcion'])?'&descripcion='.$_GET['descripcion']:'';
            echo !empty($_GET['destinatarios_id'])?'&destinatarios_id='.$_GET['destinatarios_id']:'';
            echo !empty($_GET['aplicaciones_id'])?'&aplicaciones_id='.$_GET['aplicaciones_id']:'';
        ?>';
        document.location.href="<?= base_url('food/lista') ?>"+url+"&gamas_id="+id;
    }
</script>
<script>
    $("#filterPropertyForm").hide();
</script>