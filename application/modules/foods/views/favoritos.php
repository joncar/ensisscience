<?php $this->load->view('includes/headerMain'); ?>
<div id="wrapper">
    <div id="content" class='max'>  
        <?= $this->load->view('includes/searchbox') ?>
        <div class="resultsList">
            <h1 class="pull-left">
                Mis favoritos de la lista: Todos
            </h1>
            <div class="pull-right">                
                <?php $sel = empty($_GET['foods_lista_id'])?0:$_GET['foods_lista_id']; ?>
                <?= form_dropdown_from_query('foods_lista_id', $listas->result,'id','foods_lista_nombre',$sel,'id="foods_lista_id"') ?>
                <a data-target="#addLista" data-toggle="modal" href="javascript:void(0);" id="favBtn">
                    Añadir Lista
                </a>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <?php foreach($lista->result() as $l): ?>
                <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">                    
                    <div>
                        <div align="right"><a style="color: red; font-size: 28px;" href="<?= base_url('food/rmFavoritos/'.$l->favid) ?>/"><i class="fa fa-times"></i></a></div>                        
                        <a href="<?= site_url('food/'.$l->id.'-'.toURL($l->foods_nombre)) ?>" class="card">
                            <div class="figure">
                                <div class="img" style="background-image:url(<?= base_url('img/foods/'.$l->miniatura) ?>);"></div>
                                <div class="figCaption"></div>
                                <div class="figView"><span class="icon-eye"></span></div>                            
                            </div>
                            <h2><?= $l->foods_nombre ?></h2>
                            <div class="cardAddress" style="min-height:50px;"></div>
                            <div class="cardAddress" style="text-align:right">
                                <span style="color:red">Leer más</span>
                            </div>
                            <ul class="cardFeat"></ul>
                            <div class="clearfix"></div>
                        </a>                    
                    </div>                    
                </div>
                <?php endforeach ?>
            </div>
            
                <?php if($lista->num_rows==0): ?>
                    Lo sentimos no hemos encontrado resultados para sus criterios de búsqueda
                <?php endif ?>
            <?php if($lista->num_rows>0): ?>
                <div class="pull-left">
                    <?php $this->load->view('predesign/paginacion',array('rows'=>'12','pages'=>6,'actual'=>$page,'total'=>$total_results,'url'=>'javascript:changepage({{page}})')); ?>
                </div>
                <div class="pull-right search_prop_calc">
                    <?= (($page-1)*12)==0?1:($page-1)*12 ?> - <?= (($page-1)*12)+12 ?> de <?= $total_results ?> productos
                </div>
            <?php endif ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php $this->load->view('includes/fragmentos/addLista'); ?>
<script>
    $("#foods_lista_id").change(function(){
        var str = $(this).val()!=''?'?foods_lista_id='+$(this).val():'';
        document.location.href="<?= base_url('food/favoritos') ?>"+str;
    });
</script>