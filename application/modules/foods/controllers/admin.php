<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function gamas($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','en'=>'Inglés'));
            $crud->set_field_upload('icono','img/gamas');
            $crud->set_field_upload('icono_footer','img/gamas');
            $crud->set_field_upload('icono_lista','img/gamas');
            $crud->set_field_upload('icono_app','img/gamas');
            $output = $crud->render();
            $this->loadView($output);
        }
        function aplicaciones($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','en'=>'Inglés'));
            $crud->set_field_upload('icono','img/aplicaciones');
            $output = $crud->render();
            $this->loadView($output);
        }
        function destinatarios($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','en'=>'Inglés'));            
            $output = $crud->render();
            $this->loadView($output);
        }
        function foods($x = '',$y = ''){
            $crud = $this->crud_function($x,$y);
            $crud->field_type('idioma','dropdown',array('ca'=>'Catalán','es'=>'Español','en'=>'Inglés'));
            $crud->set_field_upload('portada','img/foods');
            $crud->set_field_upload('miniatura','img/foods');
            $crud->set_field_upload('pdf','files/foods');
            $crud->set_relation_n_n('destinatarios','foods_destinatarios','destinatarios','foods_id','destinatarios_id','destinatarios_nombre','priority');
            $crud->set_relation_n_n('aplicaciones','foods_aplicaciones','aplicaciones','foods_id','aplicaciones_id','aplicaciones_nombre','priority');
            $crud->display_as('foods_nombre','Nombre')
                     ->display_as('gamas_id','Gama');
            $crud->columns('portada','gamas_id','foods_nombre','idioma','aplicaciones','destinatarios');
            $crud->add_action('<i class="fa fa-clipboard"></i> Clonar','',base_url('foods/admin/clonarFood').'/');
            $output = $crud->render();
            $this->loadView($output);
        }
        
        public function clonarFood($id){
            if(is_numeric($id)){
                $propiedad = new Bdsource();
                $propiedad->where('id',$id);
                $propiedad->init('foods',TRUE);
                $data = (array)$this->foods;
                $data['portada'] = '';
                $data['miniatura'] = '';     
                $data['pdf'] = '';                
                $propiedad->save($data,'',TRUE);
                header("Location:".base_url('foods/admin/foods/edit/'.$propiedad->getid()));
            }
        }
        
        function lista($x = '',$y = ''){
            $this->as = array('lista'=>'foods_lista');
            $crud = $this->crud_function($x,$y);
            $crud->display_as('foods_lista_nombre','Nombre');
            $crud->field_type('user_id','hidden',$this->user->id);
            $crud->columns('foods_lista_nombre');
            $crud->callback_column('foods_lista_nombre',function($val,$row){
                return '<a href="'.base_url('food/favoritos').'?foods_listas_id='.$row->id.'">'.$val.'</a>';
            });
            $crud->where('user_id',$this->user->id);
            $output = $crud->render();
            $this->loadView($output);
        }
    }
?>
