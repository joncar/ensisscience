<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Front extends Main{
        function __construct() {
            parent::__construct();            
        }
        
        function loadView($param = array('view'=>'main')){
            $data = $param;
            if(is_string($data)){
                $data = array('view'=>$data);
            }
            $data['tipo'] = 'foods';
            parent::loadView($data);
        }
        
        function index($x = '',$y = ''){
            $this->loadView(array(
                'view'=>'main',
                'dest'=>$this->db->get_where('destinatarios',array('idioma'=>$_SESSION['lang'])),
                'gamas'=>$this->db->get_where('gamas',array('idioma'=>$_SESSION['lang'])),
                'aplicaciones'=>$this->db->get_where('aplicaciones',array('idioma'=>$_SESSION['lang'])),
                'title'=>'Foods'
             ));
        }
        
        function lista(){
            
            if(empty($_GET['page'])){
                $_GET['page'] = 1;
            }
            
            $lista = new Bdsource();
            $lista->select = 'foods.id, foods.foods_nombre,foods.miniatura,foods.new, gamas.icono_lista as gamafoto, gamas.gamas_nombre';
            $limit = ($_GET['page']-1)*12;
            $limit = $limit = 0?1:$limit;
            $joins = array(
                array('foods_destinatarios','foods_destinatarios.foods_id = foods.id','left'),
                array('foods_aplicaciones','foods_aplicaciones.foods_id = foods.id','left'),
                array('gamas','gamas.id = foods.gamas_id','left')
            );            
            $lista->limit = array('12',$limit);                        
            $lista->filters = array('destinatarios_id','gamas_id','aplicaciones_id');
            
            if(!empty($_GET['gamas_id'])){
                $f = $this->db->get_where('gamas',array('id'=>$_GET['gamas_id']));
                if($f->num_rows>0){
                    $f = $f->row()->visitas+1;
                    $this->db->update('gamas',array('visitas'=>$f),array('id'=>$_GET['gamas_id']));
                }
            }
            
            if(!empty($_GET['aplicaciones_id'])){
                $f = $this->db->get_where('aplicaciones',array('id'=>$_GET['aplicaciones_id']));
                if($f->num_rows>0){
                    $f = $f->row()->visitas+1;
                    $this->db->update('aplicaciones',array('visitas'=>$f),array('id'=>$_GET['aplicaciones_id']));
                }
            }
            
            if(!empty($_GET['descripcion'])){
                $lista->where('(foods.foods_nombre like "%'.$_GET['descripcion'].'%" OR MATCH(foods.foods_nombre) AGAINST ("'.$_GET['descripcion'].'") OR MATCH(foods.descripcion) AGAINST ("'.$_GET['descripcion'].'"))',NULL);
            }
            $lista->where('foods.idioma',$_SESSION['lang']);
            $lista->group_by('foods.id');
            $lista->order_by = array('foods.orden','asc');
            $lista->joinNormal($joins);        
            $lista->where('disponible','1');
            $lista->init('foods');            
            $total_result = clone $lista;
            $total_result->select = 'foods.id, foods.gamas_id, gamas.icono_lista as gamafoto, gamas.gamas_nombre';
            $total_result->where('foods.idioma',$_SESSION['lang']);
            $total_result->limit  = array();
            $total_result->joinNormal($joins);
            $total_result->group_by('foods.id');
            $total_result->init('foods');
            
            $this->loadView(array(
                'view'=>'lista',
                'title'=>'Resultados de búsqueda',
                'lista'=>$lista,
                'page'=>$_GET['page'],
                'total'=>$total_result,
                'total_results'=>$total_result->num_rows
             ));
        }
        
        function read($id){
            if(empty($id)){
                throw new Exception('Producto no encontrado',404);
            }
            else{
                $id = explode("-",$id);
                $id = $id[0];
                if(is_numeric($id)){
                    $detail = new Bdsource('foods');
                    $detail->select = 'foods.*, gamas.gamas_nombre';
                    $detail->where('foods.id',$id);
                    $detail->innerjoin('gamas');
                    $detail->init();

                    $aplicaciones = new Bdsource('foods_aplicaciones');
                    $aplicaciones->where('foods_id',$id);
                    $aplicaciones->leftjoin('aplicaciones');
                    $aplicaciones->init();

                    $destinatarios = new Bdsource('foods_destinatarios');
                    $destinatarios->join = array('destinatarios');
                    $destinatarios->where('foods_id',$id);
                    $destinatarios->init();

                    $relacionados = new Bdsource('foods');
                    $relacionados->where('gamas_id',$detail->gamas_id);
                    $relacionados->where('foods.id != ',$detail->getid());
                    $relacionados->limit = array('4');
                    $relacionados->init();

                    $listas = new Bdsource('foods_lista');
                    $onfav = new Bdsource('foods_favoritos');
                    if(!empty($_SESSION['user'])){
                        $listas->where('user_id',$this->user->id); 
                        $listas->init();
                        $onfav->where(array('user_id'=>$this->user->id,'foods_id'=>$id));
                        $onfav->init();
                    }else{
                        $listas->where('foods_lista.id',-1);
                        $listas->init();
                        $onfav->where(array('user_id'=>-1));
                        $onfav->init();
                    }                    
                    
                    $onfav = $onfav->num_rows==0?FALSE:TRUE;

                        $this->loadView(array(
                            'view'=>'read',
                            'detail'=>$detail,
                            'destinatarios'=>$destinatarios,
                            'aplicaciones'=>$aplicaciones,
                            'title'=>$detail->foods_nombre,
                            'relacionados'=>$relacionados,
                            'listas'=>$listas,
                            'id'=>$id,
                            'onfav'=>$onfav
                        ));
                }else{
                    throw new Exception('Producto no encontrado',404);
                }
            }
        }
        
        function favoritos(){
            if(empty($_SESSION['user'])){
                header("Location:".base_url('panel')); 
                exit;
            }
            if(empty($_GET['page'])){
                $_GET['page'] = 1;
            }
            
            $lista = new Bdsource();
            $lista->select = 'foods.*, foods_favoritos.id as favid';
            $limit = ($_GET['page']-1)*12;
            $limit = $limit = 0?1:$limit;
            $joins = array(
                array('foods_destinatarios','foods_destinatarios.foods_id = foods.id'),
                array('foods_aplicaciones','foods_aplicaciones.foods_id = foods.id'),
                array('foods_favoritos','foods_favoritos.foods_id = foods.id')
            );            
            $lista->limit = array('12',$limit);                        
            $lista->filters = array('foods_lista_id');
            $lista->group_by('foods.id');
            $lista->joinNormal($joins);
            $lista->where('foods_favoritos.user_id',$this->user->id);
            $lista->init('foods');
            
            $total_result = clone $lista;
            $total_result->limit  = array();
            $total_result->joinNormal($joins);
            $total_result->group_by('foods.id');
            $total_result->where('foods_favoritos.user_id',$this->user->id);
            $total_result->init('foods');
            
            $listas = new Bdsource('foods_lista');
            $listas->where('user_id',$this->user->id); 
            $listas->init();
            $this->loadView(array(
                'view'=>'favoritos',
                'title'=>'Mis Favoritos',
                'listas'=>$listas,
                'lista'=>$lista,
                'page'=>$_GET['page'],
                'total_results'=>$total_result->num_rows
             ));
        }
        
        function addLista(){            
            $this->form_validation->set_rules('foods_lista_nombre','Nombre','required');
            if($this->form_validation->run()){                
                    $fav = new Bdsource('foods_lista');
                    $fav->foods_lista_nombre = $this->input->post('foods_lista_nombre');
                    $fav->user_id = $this->user->id;
                    $fav->save(array(),$_POST['id']);
                    $listas = new Bdsource('foods_lista');
                    $listas->where('user_id',$this->user->id); 
                    $listas->init();
                    echo $this->traduccion->traducir($this->load->view('includes/fragmentos/_addLista',array('listas'=>$listas,'in'=>true),true),$_SESSION['lang']);
            }
        }
        
        function rmLista(){            
            $this->form_validation->set_rules('id','ID','required|greather_than[0]');
            if($this->form_validation->run()){                
                    $fav = new Bdsource('foods_lista');
                    $fav->remove($_POST['id']);
                    $fav = new Bdsource('foods_favoritos');
                    $fav->where('foods_favoritos.foods_lista_id',$_POST['id']);
                    $fav->remove();
                    $listas = new Bdsource('foods_lista');
                    $listas->where('user_id',$this->user->id); 
                    $listas->init();                    
                    echo $this->traduccion->traducir($this->load->view('includes/fragmentos/_addLista',array('listas'=>$listas,'in'=>true),true),$_SESSION['lang']);
            }
        }
        
        function refreshFavoritos($id){            
            $listas = new Bdsource('foods_lista');
            $listas->where('user_id',$this->user->id); 
            $listas->init();
            echo $this->traduccion->traducir($this->load->view('includes/fragmentos/_addFavorito',array('listas'=>$listas,'id'=>$id),true),$_SESSION['lang']);
        }
        
        function addFavoritos(){
            $this->form_validation->set_rules('foods_id','Foods ID','required|greather_than[0]');
            $this->form_validation->set_rules('foods_lista_id','Foods ID','required|greather_than[0]');
            if($this->form_validation->run()){
                $existe = new Bdsource('foods_favoritos');
                $existe->where(array('foods_id'=>$_POST['foods_id'],'user_id'=>$this->user->id,'foods_lista_id'=>$_POST['foods_lista_id']));
                $existe->init();
                if($existe->num_rows==0){
                    $fav = new Bdsource('foods_favoritos');
                    $fav->foods_id = $this->input->post('foods_id');
                    $fav->foods_lista_id = $this->input->post('foods_lista_id');
                    $fav->user_id = $this->user->id;
                    $fav->save();
                }
            }
        }
        
        function rmFavoritos($id){            
            $_POST['id'] = $id;
            $this->form_validation->set_rules('id','Foods ID','required|greather_than[0]');
            if($this->form_validation->run()){
                $fav = new Bdsource('foods_favoritos');
                $fav->remove($_POST['id']);
            }
            header("Location:".base_url('food/favoritos'));
        }
        
        function contacto(){
            parent::contacto();
        }
        function empresa(){
            parent::empresa();
        }
    }
?>
