<!DOCTYPE html>
<html lang="es-ES">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=1">
        <title><?= empty($title)?'Ensis Sciences':$title ?></title>
        <script src="http://code.jquery.com/jquery-1.10.0.js"></script>
        <link rel="shortcut icon" href="<?= base_url('img/favicon.png') ?>" type="image/x-icon" />
        <link rel='stylesheet' id='dsidx-css'  href='http://cdn3.diverse-cdn.com/api/combo-css/config=dsidxpress.css/43e53d' type='text/css' media='all' />
        <link rel='stylesheet' id='open_sans-css'  href='https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C700&#038;subset=latin%2Cgreek%2Cgreek-ext%2Cvietnamese%2Ccyrillic-ext%2Clatin-ext%2Ccyrillic&#038;ver=1.0' type='text/css' media='all' />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.2.3/css/simple-line-icons.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">                
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <link rel='stylesheet' id='fancybox-css'  href='http://mariusn.com/themes/reales-wp/wp-content/themes/reales/css/jquery.fancybox.css' type='text/css' media='all' />
        <link rel='stylesheet' id='fancybox_buttons-css'  href='http://mariusn.com/themes/reales-wp/wp-content/themes/reales/css/jquery.fancybox-buttons.css?ver=1.0' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= base_url('files/flaticons/flaticon.css') ?>?ver=1.0' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= base_url('files/flaticonsdos/flaticondos.css') ?>?ver=1.0' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= base_url('files/fonticonstres/flaticon.css') ?>?ver=1.0' type='text/css' media='all' />
        <link rel='stylesheet' href='<?= base_url('files/foodicon/flaticon.css') ?>?ver=1.0' type='text/css' media='all' />
        <link rel='stylesheet' id='reales_style-css'  href='<?= base_url('css/template.css') ?>?ver=1.0' type='text/css' media='all' />
        <?php if(!empty($tipo)):?>
            <link rel='stylesheet' href='<?= base_url('css/style-'.$tipo.'.css') ?>' type='text/css' media='all' />
        <?php else:?>
            <link rel='stylesheet' href='<?= base_url('css/style-foods.css') ?>' type='text/css' media='all' />
        <?php endif ?>        
        
    </head>
    <body>        
        <?= $this->load->view($view) ?>
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery-placeholder/2.3.1/jquery.placeholder.min.js'></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.15/jquery.touchSwipe.min.js'></script>
        <script type='text/javascript' src='https://cdn.jsdelivr.net/jquery.ui.touch-punch/0.2.3/jquery.ui.touch-punch.min.js'></script>
        <script type='text/javascript' src='https://cdn.jsdelivr.net/slimscroll/1.3.7/jquery.slimscroll.min.js'></script>                
        <script type='text/javascript' src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>                
        <script type='text/javascript' src='http://mariusn.com/themes/reales-wp/wp-content/themes/reales/js/jquery.fancybox.js?ver=2.1.5'></script>
        <script type='text/javascript' src='<?= base_url('js/image-scale.min.js') ?>?ver=1.0'></script>
        <script type='text/javascript'>
            /* <![CDATA[ */
            var services_vars = {"signup_loading": "Sending...", "signup_text": "Sign Up", "signin_loading": "Sending...", "signin_text": "Sign In", "forgot_loading": "Sending...", "forgot_text": "Get New Password", "reset_pass_loading": "Sending...", "reset_pass_text": "Reset Password", "fb_login_loading": "Sending...", "fb_login_text": "Sign In with Facebook", "fb_login_error": "Login cancelled or not fully authorized!", "google_signin_loading": "Sending...", "google_signin_text": "Sign In with Google", "google_signin_error": "Signin cancelled or not fully authorized!", "search_id": "", "search_country": "", "search_state": "", "search_city": "", "search_category": "0", "search_type": "0", "search_min_price": "", "search_max_price": "", "search_lat": "37.77024821511303", "search_lng": "-122.44061568005372", "search_bedrooms": "", "search_bathrooms": "", "search_neighborhood": "", "search_min_area": "", "search_max_area": "", "search_unit": "sq ft", "search_amenities": [], "sort": "newest", "default_lat": "37.77024821511303", "default_lng": "-122.44061568005372", "zoom": "13", "infobox_close_btn": "Close", "infobox_view_btn": "View", "page": "0", "post_id": "2", "user_id": "0", "update_property": "Update Property", "marker_color": "#0eaaa6", "saving_property": "Saving Property...", "deleting_property": "Deleting Property...", "please_wait": "Please wait...", "featuring_property": "Setting Property as Featured...", "home_redirect": "http:\/\/mariusn.com\/themes\/reales-wp", "list_redirect": "http:\/\/mariusn.com\/themes\/reales-wp\/my-properties\/", "send_message": "Send Message", "sending_message": "Sending Message...", "save": "Save", "saving": "Saving...", "updating_profile": "Updating Profile...", "use_captcha": "", "use_submit_captcha": "", "gmaps_style": "%5B%7B%22featureType%22%3A%22water%22%2C%22elementType%22%3A%22geometry.fill%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23d3d3d3%22%7D%5D%7D%2C%7B%22featureType%22%3A%22transit%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23808080%22%7D%2C%7B%22visibility%22%3A%22off%22%7D%5D%7D%2C%7B%22featureType%22%3A%22road.highway%22%2C%22elementType%22%3A%22geometry.stroke%22%2C%22stylers%22%3A%5B%7B%22visibility%22%3A%22on%22%7D%2C%7B%22color%22%3A%22%23b3b3b3%22%7D%5D%7D%2C%7B%22featureType%22%3A%22road.highway%22%2C%22elementType%22%3A%22geometry.fill%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23ffffff%22%7D%5D%7D%2C%7B%22featureType%22%3A%22road.local%22%2C%22elementType%22%3A%22geometry.fill%22%2C%22stylers%22%3A%5B%7B%22visibility%22%3A%22on%22%7D%2C%7B%22color%22%3A%22%23ffffff%22%7D%2C%7B%22weight%22%3A1.8%7D%5D%7D%2C%7B%22featureType%22%3A%22road.local%22%2C%22elementType%22%3A%22geometry.stroke%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23d7d7d7%22%7D%5D%7D%2C%7B%22featureType%22%3A%22poi%22%2C%22elementType%22%3A%22geometry.fill%22%2C%22stylers%22%3A%5B%7B%22visibility%22%3A%22on%22%7D%2C%7B%22color%22%3A%22%23ebebeb%22%7D%5D%7D%2C%7B%22featureType%22%3A%22administrative%22%2C%22elementType%22%3A%22geometry%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23a7a7a7%22%7D%5D%7D%2C%7B%22featureType%22%3A%22road.arterial%22%2C%22elementType%22%3A%22geometry.fill%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23ffffff%22%7D%5D%7D%2C%7B%22featureType%22%3A%22road.arterial%22%2C%22elementType%22%3A%22geometry.fill%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23ffffff%22%7D%5D%7D%2C%7B%22featureType%22%3A%22landscape%22%2C%22elementType%22%3A%22geometry.fill%22%2C%22stylers%22%3A%5B%7B%22visibility%22%3A%22on%22%7D%2C%7B%22color%22%3A%22%23efefef%22%7D%5D%7D%2C%7B%22featureType%22%3A%22road%22%2C%22elementType%22%3A%22labels.text.fill%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23696969%22%7D%5D%7D%2C%7B%22featureType%22%3A%22administrative%22%2C%22elementType%22%3A%22labels.text.fill%22%2C%22stylers%22%3A%5B%7B%22visibility%22%3A%22on%22%7D%2C%7B%22color%22%3A%22%23737373%22%7D%5D%7D%2C%7B%22featureType%22%3A%22poi%22%2C%22elementType%22%3A%22labels.icon%22%2C%22stylers%22%3A%5B%7B%22visibility%22%3A%22off%22%7D%5D%7D%2C%7B%22featureType%22%3A%22poi%22%2C%22elementType%22%3A%22labels%22%2C%22stylers%22%3A%5B%7B%22visibility%22%3A%22off%22%7D%5D%7D%2C%7B%22featureType%22%3A%22road.arterial%22%2C%22elementType%22%3A%22geometry.stroke%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23d6d6d6%22%7D%5D%7D%2C%7B%22featureType%22%3A%22road%22%2C%22elementType%22%3A%22labels.icon%22%2C%22stylers%22%3A%5B%7B%22visibility%22%3A%22off%22%7D%5D%7D%2C%7B%7D%2C%7B%22featureType%22%3A%22poi%22%2C%22elementType%22%3A%22geometry.fill%22%2C%22stylers%22%3A%5B%7B%22color%22%3A%22%23dadada%22%7D%5D%7D%5D", "loading_searches": "Loading Searches List...", "no_searches": "Searches list empty."};
            var main_vars = {"no_city": "Please set location", "max_price": "2500000", "max_area": "5000", "currency": "$", "currency_pos": "before", "unit": "sq ft", "search_placeholder": "Search for...", "top_admin_menu": "", "idx_search_location": "Location", "idx_search_category": "Category", "idx_search_price_min": "Min price", "idx_search_price_max": "Max price", "idx_search_beds": "Bedrooms", "idx_search_baths": "Bathrooms", "idx_advanced_search": "Advanced Search", "idx_advanced_filter": "Show advanced search options", "idx_advanced_filter_hide": "Hide advanced search options"};
            /* ]]> */
        </script>
        <script type='text/javascript' src='<?= base_url('js/services.js') ?>?ver=1.0'></script>        
        <script type='text/javascript' src='<?= base_url('js/main.js') ?>?ver=1.0'></script>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery.devbridge-autocomplete/1.2.24/jquery.autocomplete.js'></script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-42697305-1', 'auto');
            ga('send', 'pageview');

        </script>
    </body>
</html>