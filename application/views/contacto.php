<div id="page-hero-container">
    <div class="page-hero" style="background-image:url(<?= base_url('img/paginas').'/'.$p->fondo ?>)"></div>
    <div class="slideshowShadow"></div>
    <div class="home-header menuontop" id='menu'>
        <div class="col-xs-12" id="homelogo2" style="text-align:center;">
             <a href="<?= site_url('food') ?>">
                <?= img('img/logo.png','width:20%') ?>
            </a>
        </div>
        <div class="col-lg-3 hidden-xs home-logo osLight" id="homelogo">
            <a href="<?= site_url('food') ?>">
                <?= img('img/logo.png') ?>
            </a>
        </div>
        
        <?php $this->load->view('includes/fragmentos/menu'); ?>
    </div>
    <div class="page-caption">
        <div class="page-title">Contáctenos</div>
    </div>
</div>


<div  id="page"  class="page-wrapper">
    <div class="page-content">
        <div class="row" style="margin-left:0px; margin-right:0px">
            <div class="col-xs-12 col-sm-12">
                <div>
                    <h2 class="pageHeader">Ensis Sciences</h2>
                    <div class="row pb20">
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <h4>Detalles de contacto</h4>
                            <div class="contact-details"><span class="contact-icon fa fa-phone"></span>  <?= $this->ajustes->telefono ?></div>                            
                            <div class="contact-details"><a href="mailto:<?= $this->ajustes->correo ?>"><span class="contact-icon fa fa-envelope-o"></span> <?= $this->ajustes->correo ?></a></div>
                            <div class="contact-details"><span class="contact-icon fa fa-map-marker"></span> <?= $this->ajustes->direccion_contacto ?></div>
                            <img src="http://www.ensissciences.com/webnew/img/Logo_EsadeCreapolis_CMYK.jpg" style="">  
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                            <h4>Síguenos</h4>
                            <div class="contact-details"><a href="<?= $this->ajustes->facebook ?>" class="text-facebook"><span class="contact-icon fa fa-facebook"></span> Facebook</a></div>
                            <div class="contact-details"><a href="<?= $this->ajustes->twitter ?>" class="text-twitter"><span class="contact-icon fa fa-twitter"></span> Twitter</a></div>
                          
                            <div class="contact-details"><a href="<?= $this->ajustes->linkedin ?>" class="text-linkedin"><span class="contact-icon fa fa-linkedin"></span> LinkedIn</a></div>
                        </div>
                    </div>
                  
 <div class="entry-content">
                        <p><?= $this->ajustes->texto_contacto ?></p>
                        <div class="clearfix"></div>
                    </div>

                    <h4>Contáctenos</h4>
                    <form id="formcontact" class="contactPageForm" method="post" onsubmit="return sendMessage()">                        
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="cp_response"></div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="cp_name">Nombre <span class="text-red">*</span></label>
                                    <input id="nombre" name="nombre"  placeholder="Escriba su nombre" class="form-control" type="text" >
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <div class="form-group">
                                    <label for="cp_email">Email <span class="text-red">*</span></label>
                                    <input id="email" name="email" placeholder="Escriba su email" class="form-control" type="email" >
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="cp_message">Mensaje <span class="text-red">*</span></label>
                                    <textarea id="mensaje" name="mensaje" placeholder="Escriba su mensaje" rows="3" class="form-control" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <button class="btn btn-green">Enviar mensaje</button>
                                </div>
                            </div>
                        </div>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class='page-wrapper' style="padding:40px 0 0; margin:0 ; width:100%;">
<h2 id="aplicaciones" class="centered osLight">Ubicación</h2>
    <?php $coords = maptoarray($this->ajustes->mapa_contacto); $cord = $coords[0].','.$coords[1]; ?>    
    <div id='mapa' style="width:100%; height:300px;"></div>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyB_R8j4rq6MXoV17xrbyyh2rwZS6pTMd9w&libraries=drawing,geometry,places"></script>
    <script>
        var mapaContent = document.getElementById('mapa');
        var center = new google.maps.LatLng(<?= $cord ?>);    
        var mapOptions = {
                    zoom: 15,
                    center: center,               
        };
        var map = new google.maps.Map(mapaContent, mapOptions);
        new google.maps.Marker({position: new google.maps.LatLng(<?= $cord ?>),map:map});
    </script>
</div>
<?php if(isset($this->gamas)): ?>
<?= $this->load->view('includes/footer') ?>
<?php endif ?>
<script>
    $(document).ready(function () {
        $(window).on('scroll', function () {
            if ($(this).scrollTop() > $("#page").position().top - 50)
                $("#menu").addClass('menuonbottom').removeClass('menuontop');
            else
                $("#menu").addClass('menuontop').removeClass('menuonbottom');
        });

        $("a[href*=#]").click(function (e) {
            e.preventDefault();
            var target = $(this).attr('href');
            target = target.split("#");
            target = $("#" + target[1]);

            $('html, body').stop().animate({'scrollTop': parseInt(target.offset().top) - 30}, 900, 'swing');
        })
    });
    
    function sendMessage(){        
        if($("#nombre").val()!=='' && $("#email").val()!=='' && $("#mensaje").val()!==''){
            var datos = document.getElementById('formcontact');
            var f = new FormData(datos);
            console.log(f);
            $.ajax({
                url:'<?= base_url('api/contactar') ?>',
                data:f,
                cache: false,
                contentType: false,
                processData: false,
                type:'POST',
                success:function(data){
                    alert('Su mensaje ha sido enviado con éxito');
                }
            });
        }else{
            alert('por favor complete los datos, para poder contactarlo.');
        }
        return false;
    }
</script>