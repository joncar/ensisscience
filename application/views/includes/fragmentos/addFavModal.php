<div class="modal fade" id="addFavorito" role="dialog" aria-labelledby="searches-label" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="fa fa-close"></span></button>
                <h4 class="modal-title" id="searches-label">Mis búsquedas</h4>
            </div>
            <div class="modal-body"></div>
            <input type="hidden" name="modal-user-id" id="modal-user-id" value="0">
            <input type="hidden" id="securityDeleteSearch" name="securityDeleteSearch" value="c7c8fbceae" /><input type="hidden" name="_wp_http_referer" value="/themes/reales-wp/" />            <div class="modal-footer">
                <a href="javascript:void(0);" data-dismiss="modal" class="btn btn-gray">Cerrar</a>
            </div>
        </div>
    </div>
</div>