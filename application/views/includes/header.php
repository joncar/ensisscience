<header id="navbar" class="navbar navbar-default">
    <script type="text/javascript">
            try{ace.settings.check('navbar' , 'fixed')}catch(e){}
    </script>

    <div class="navbar-container" id="navbar-container">
            <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
                    <span class="sr-only">Toggle sidebar</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>
            </button>

            <div class="navbar-header pull-left">
                    <a href="<?= site_url() ?>" class="navbar-brand">
                            <small>
                                    <!--<img src="<?= base_url('images/logo.png') ?>" style="width:50%;">-->
                                <i class="fa fa-home"></i>
                            </small>
                    </a> 
            </div>            
            <div class="navbar-buttons navbar-header pull-right" role="navigation">
                    <ul class="nav ace-nav">
                        <?php if($this->user->log): ?>
                            <li class="light-blue">
                                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                            <img class="nav-user-photo" src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="<?= $this->user->nombre ?>" />
                                            <span class="user-info">
                                                    <small>Bienvenido</small>
                                                    <?= $this->user->nombre ?>
                                            </span>
                                            <i class="ace-icon fa fa-caret-down"></i>
                                    </a>

                                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                                            <li>
                                                    <a href="<?= base_url('seguridad/perfil/edit/'.$this->user->id) ?>">
                                                            <i class="ace-icon fa fa-user"></i>
                                                            Perfil
                                                    </a>
                                            </li>
                                            <li>
                                                    <a href="<?= base_url('main/unlog') ?>">
                                                            <i class="ace-icon fa fa-power-off"></i>
                                                            Salir
                                                    </a>
                                            </li>
                                    </ul>
                            </li>
                        <?php endif ?>
                            <li class="light-blue">
                                    <a href="<?= site_url('') ?>" class="btn" style="margin-top:0">
                                            <span class="user-info">
                                                    <small>Ir a la web</small>
                                            </span>
                                    </a>
                            </li>
                    </ul>
            </div>            
    </div><!-- /.navbar-container -->
</header>
