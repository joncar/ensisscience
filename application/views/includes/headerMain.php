<div id="header">
    <div class="logo">
        <a href="<?= site_url() ?>">
            <span class="fa fa-home marker"></span><span class="logoText">Ensis Sciences</span>
        </a>
    </div>
    <a href="javascript:void(0);" class="navHandler"><span class="fa fa-ellipsis-v"></span></a>
    <a href="javascript:void(0);" class="mapHandler"><span class="icon-map"></span></a>
    <?= $this->load->view('includes/fragmentos/menu') ?>
    <div class="clearfix"></div>
</div>

<div id="leftSide">
    <nav class="leftNav scrollable">
        <div class="menu-leftside-menu-container">
            <ul class="menu">
                <li class="icon-plus">
                    <a href="<?= base_url('panel') ?>">
                        <span class="navIcon"></span><span class="navLabel">Entrar</span>
                    </a>
                </li>
                <?php if(!empty($_SESSION['user'])): ?>
                    <li class="icon-heart">
                        <a href="<?= base_url('food/favoritos') ?>">
                            <span class="navIcon"></span>
                            <span class="navLabel">Favoritos</span>
                        </a>
                    </li>
                <?php else: ?>
                    <li class="icon-heart">
                        <a href="<?= base_url('registro/index/add') ?>?redirect=<?= base_url('food/favoritos') ?>">
                            <span class="navIcon"></span>
                            <span class="navLabel">Favoritos</span>
                        </a>
                    </li>
                <?php endif ?>
            </ul>
        </div>    
    </nav>
</div>
<div class="closeLeftSide"></div>