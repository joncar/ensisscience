<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
           <li>
                <a class="dropdown-toggle" href="#">
                    <i class="menu-icon fa fa-cutlery"></i>
                    <span class="menu-text">Foods</span>
                    <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class='submenu'>
                    <li>
                        <a href="<?= base_url('foods/admin/lista') ?>">
                            <i class="menu-icon fa fa-list"></i>
                            <span class="menu-text">Mis Listas</span>
                        </a>                                
                    </li>                    
                </ul>
            </li>
            <?php if($this->user->admin==1): ?>
            <!--- Admin --->
            <li>
                <a class="dropdown-toggle" href="#">
                        <i class="menu-icon fa fa-lock"></i>
                        <span class="menu-text">Admin</span>
                        <b class="arrow fa fa-angle-down"></b>
                </a>
                <b class="arrow"></b>
                <ul class="submenu">
                    <li>
                        <a href="<?= base_url('admin/ajustes') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Ajustes</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/notificaciones') ?>">
                            <i class="menu-icon fa fa-envelope"></i>
                            <span class="menu-text">Notificaciones</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/paginas') ?>">
                            <i class="menu-icon fa fa-users"></i>
                            <span class="menu-text">Páginas</span>                                    
                        </a>                                
                    </li>
                    <li>
                        <a href="<?= base_url('admin/opiniones') ?>">
                            <i class="menu-icon fa fa-chat"></i>
                            <span class="menu-text">Opiniones</span>                                    
                        </a>                                
                    </li>
                    <!--- Comidas ---->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Foods</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('foods/admin/gamas') ?>">
                                    <i class="menu-icon fa fa-users"></i>
                                    <span class="menu-text">Gamas</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('foods/admin/destinatarios') ?>">
                                    <i class="menu-icon fa fa-arrows"></i>
                                    <span class="menu-text">Destinatarios</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('foods/admin/aplicaciones') ?>">
                                    <i class="menu-icon fa fa-user"></i>
                                    <span class="menu-text">Aplicaciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('foods/admin/foods') ?>">
                                    <i class="menu-icon fa fa-cutlery"></i>
                                    <span class="menu-text">Foods</span>                                    
                                </a>                                
                            </li>
                        </ul>
                    </li>
                    
                    <!--- Beverages ---->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-beer"></i>
                            <span class="menu-text">Beverages</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('beverages/admin/tipos') ?>">
                                    <i class="menu-icon fa fa-beer"></i>
                                    <span class="menu-text">Tipo beverage</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('beverages/admin/clasificacion') ?>">
                                    <i class="menu-icon fa fa-beer"></i>
                                    <span class="menu-text">Clasificación</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('beverages/admin/beverage_funciones') ?>">
                                    <i class="menu-icon fa fa-beer"></i>
                                    <span class="menu-text">Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('beverages/admin/clases_funciones') ?>">
                                    <i class="menu-icon fa fa-beer"></i>
                                    <span class="menu-text">Clases de Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('beverages/admin/tipos_funciones') ?>">
                                    <i class="menu-icon fa fa-beer"></i>
                                    <span class="menu-text">Tipos Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('beverages/admin/beverages') ?>">
                                    <i class="menu-icon fa fa-beer"></i>
                                    <span class="menu-text">Beverages</span>                                    
                                </a>                                
                            </li>
                        </ul>
                    </li>
                    <!--- Seguridad --->
                    <li>
                        <a class="dropdown-toggle" href="#">
                            <i class="menu-icon fa fa-lock"></i>
                            <span class="menu-text">Seguridad</span>
                            <b class="arrow fa fa-angle-down"></b>
                        </a>
                        <b class="arrow"></b>
                        <ul class='submenu'>
                            <li>
                                <a href="<?= base_url('seguridad/grupos') ?>">
                                    <i class="menu-icon fa fa-users"></i>
                                    <span class="menu-text">Grupos</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/funciones') ?>">
                                    <i class="menu-icon fa fa-arrows"></i>
                                    <span class="menu-text">Funciones</span>                                    
                                </a>                                
                            </li>
                            <li>
                                <a href="<?= base_url('seguridad/user') ?>">
                                    <i class="menu-icon fa fa-user"></i>
                                    <span class="menu-text">Usuarios</span>                                    
                                </a>                                
                            </li>
                        </ul>
                    </li>
                    <!--- Fin seguridad ---->
                </ul>
            </li>
            <!--- Fin Admin ---->
            <?php endif ?>
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed');
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
