<?php $this->load->view('includes/headerMain'); ?>
<div id="wrapper">
        <div class="container"><!-- container via hooks -->	<div id="page-content-container">	
            <div class="row-fluid">
                <div class="col-xs-12 col-sm-4 col-sm-offset-1">
                    <div class="form-container">
                        <?= $this->load->view('predesign/login') ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-sm-offset-1">
                    <div class="form-container">
                        <?= $output ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>