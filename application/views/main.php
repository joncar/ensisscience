<?php redirect('food'); ?>
<div id="hero-container" style="height:100%;" clas="mainHeight">
    <div id="slideshow" class="mainHeight">        
        <div class="pattern"></div>
        <video autoplay="true" muted="true" loop="true">
            <source src="<?= base_url() ?>video/intro.ogv" type="video/ogg">
            <source src="<?= base_url() ?>video/intro.mp4" type="video/mp4">
        </video>
    </div>
    <div class="slideshowShadow mainHeight" style="z-index:0"></div>
    <div style="background: transparent linear-gradient(to bottom, rgba(255, 255, 255, 0.94) 0%, rgba(238, 238, 238, 0) 100%) repeat scroll 0% 0%; z-index: 100000; position: relative; padding:0 20px;" class="no-touch">
            <div class="row"  style="margin-left:0px; margin-right:0px;">
                <h2 class="osLight centered" style="margin-bottom: -19px; position:relative; margin-top:10px;">
                        <?= img('img/logo3.png','max-width:80%') ?>
                </h2>
        <div style="margin-bottom:40px; font-weight: 100; color:black; font-style:italic; font-size: 20px; text-align:center;">
          SOLUTIONS WITH FUNCTIONAL INGREDIENTS
      </div>
                <div class="col-xs-5 col-sm-5 col-md-5  col-lg-3  col-lg-offset-2 s-menu-item">
                    <a href="<?= site_url('food') ?>">
                        <span class="flaticonfood-food s-icon"></span>
                        <div class="s-content">
                            <h2 class="centered s-main osLight">Food</h2>
                            <h3 class="s-sub osLight">Enter / Entrar</h3>
                        </div>
                    </a>
                </div>
                <div class="col-xs-5 col-sm-5 col-md-5 col-xs-offset-2 col-sm-6 col-lg-3 col-lg-offset-2 s-menu-item beverage">
                    <a href="<?= site_url('beverage') ?>">
                        <span class="flaticon-alcohol149 s-icon"></span>
                        <div class="s-content">
                            <h2 class="centered s-main osLight">Beverages</h2>
                            <h3 class="s-sub osLight" style="color:#999 !important">Enter / Entrar</h3>
                        </div>
                    </a>
                </div>
                <!--<div align="center" class="visible-xs" style="color:white; margin-top: 520px;;">© 2016 Ensis Sciences. Spain. All rights reserved</div>-->
            </div>  
            

                   
    </div>    
    <div align="center" class="" style="position:absolute; z-index: 10001; bottom:10px; width:100%; color:white;">© 2016 Ensis Sciences. Spain. All rights reserved</div>
</div>
