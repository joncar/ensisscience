<div id="page-hero-container">
    <div class="page-hero" style="background-image:url(<?= base_url('img/paginas/recover.png') ?>)"></div>
    <div class="slideshowShadow"></div>
    <div class="home-header menuontop" id='menu'>
        <div class="col-xs-12" id="homelogo2" style="text-align:center;">
             <a href="<?= site_url('food') ?>">
                <?= img('img/logo.png','width:20%') ?>
            </a>
        </div>
        <div class="col-lg-3 hidden-xs home-logo osLight" id="homelogo">
            <a href="<?= site_url('food') ?>">
                <?= img('img/logo.png') ?>
            </a>
        </div>
        
        <?php $this->load->view('includes/fragmentos/menu'); ?>
    </div>
    <div class="page-caption">
        <div class="page-title">Recuperar contraseña</div>
    </div>
</div>


<div  id="page"  class="page-wrapper">
    <div class="container">
        <section class="container-fluid" style="margin-right: 0px; margin-right:0px;">
            <div class="col-sm-4 col-sm-offset-4 col-xs-12">
                <form action="<?= base_url('registro/forget') ?>" method="post" onsubmit="return validar(this)" role="form" class="form-horizontal">
                    <?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?>
                    <?= !empty($msj)?$msj:'' ?>
                    <?= input('email','Email','email') ?>        
                    <div align='center'><button type="submit" class="btn btn-success">Recuperar contraseña</button></div>
                </form>
            </div>    
        </section>
    </div>
</div>
<?php if(isset($this->gamas)): ?>
<?= $this->load->view('includes/footer') ?>
<?php endif ?>