controllers.controller('Aplicaciones', function($scope,$rootScope,$http,Api) {
    $scope.urlImg = ServerUrl+'img/foods/';
    $scope.urlIco = ServerUrl+'img/aplicaciones/';
    $scope.aplicaciones = [];
    $scope.loadingSpinner = true;
    $scope.showed = '';    
    
    $scope.consultar = function(){
        Api.list('aplicaciones',{},$scope,$http,function(data){
            $scope.aplicaciones = [];
            for(var i=data.length-1;i>=0;i--){
                data[i].productos = JSON.parse(data[i].productos);
                $scope.aplicaciones.push(data[i]);
            }
            //Cachear los datos
            Api.aplicaciones = $scope.aplicaciones;
            localStorage.aplicaciones = JSON.stringify($scope.aplicaciones);
            $scope.loadingSpinner = false;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };
    
    if(localStorage.aplicaciones===undefined){
        $scope.consultar();
    }else{
        $scope.aplicaciones = JSON.parse(localStorage.aplicaciones);
        Api.aplicaciones = $scope.aplicaciones;
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loadingSpinner = false;
    }
    
     $scope.shownGroup = [];
    $scope.toggleGroup = function(group) {
        if($scope.isGroupShown(group)){
            $scope.shownGroup[group.id] = false;
        }else{
            $scope.shownGroup[group.id] = true;
        }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup[group.id]?true:false;
    };
    
    $scope.searchInfo = {};    
    $scope.$on('search',function(evt,data){
        $scope.searchInfo = data;
    });
    
    $scope.producto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };
});