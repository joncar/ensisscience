controllers.controller('Cervezas', function($scope,$rootScope,$http,Api) {
    $scope.urlImg = ServerUrl+'img/beverages/';
    $scope.urlIco = ServerUrl+'img/clasificaciones/';
    //Traer destinatarios
    $scope.cervezas = [];
    $scope.loadingSpinner = true;
    $scope.showed = '';
    
    $scope.consultar = function(){
        Api.list('clasificacion',{tipos_id:2},$scope,$http,function(data){
            $scope.cervezas = [];
            for(var i=data.length-1;i>=0;i--){
                data[i].productos = JSON.parse(data[i].productos);
                $scope.cervezas.push(data[i]);
            }
            //Cachear los datos
            Api.cervezas = $scope.cervezas;
            localStorage.cervezas = JSON.stringify($scope.cervezas);
            $scope.loadingSpinner = false;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };    
    if(localStorage.cervezas===undefined){
        $scope.consultar();
    }else{
        $scope.cervezas = JSON.parse(localStorage.cervezas);
        Api.cervezas = $scope.cervezas;
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loadingSpinner = false;
    }
    
     $scope.shownGroup = [];
    $scope.toggleGroup = function(group) {
        if($scope.isGroupShown(group)){
            $scope.shownGroup[group.id] = false;
        }else{
            $scope.shownGroup[group.id] = true;
        }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup[group.id]?true:false;
    };
    
    $scope.searchInfo = {};
    $scope.$on('search',function(evt,data){
        $scope.searchInfo = data;
    });
    
    $scope.producto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };
});