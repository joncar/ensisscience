controllers.controller('Funciones', function($scope,$rootScope,$http,Api) {
    $scope.urlImg = ServerUrl+'img/beverages/';
    $scope.urlIco = ServerUrl+'img/beverage_funciones/';
    //Traer destinatarios
    $scope.funciones = [];
    $scope.loadingSpinner = true;
    $scope.showed = '';
    
    $scope.consultar = function(){
        Api.list('funciones',{},$scope,$http,function(data){
            $scope.funciones = [];
            for(var i=data.length-1;i>=0;i--){
                data[i].productos = JSON.parse(data[i].productos);
                $scope.funciones.push(data[i]);
            }
            //Cachear los datos
            Api.funciones = $scope.funciones;
            localStorage.funciones = JSON.stringify($scope.funciones);
            $scope.loadingSpinner = false;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };    
    if(localStorage.funciones===undefined){
        $scope.consultar();
    }else{
        $scope.funciones = JSON.parse(localStorage.funciones);
        Api.funciones = $scope.funciones;
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loadingSpinner = false;
    }
    
    $scope.shownGroup = [];
    $scope.toggleGroup = function(group) {
        if($scope.isGroupShown(group)){
            $scope.shownGroup[group.id] = false;
        }else{
            $scope.shownGroup[group.id] = true;
        }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup[group.id]?true:false;
    };
    
    $scope.searchInfo = {};
    $scope.$on('search',function(evt,data){
        $scope.searchInfo = data;
    });
    
    $scope.producto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };
});