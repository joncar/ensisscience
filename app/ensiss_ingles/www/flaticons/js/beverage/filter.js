MapApp.filter('productSearch', function() {
    //tipos   
    function _buscarTipo(productos,search){
        var p = [];        
        if(parseInt(search)===parseInt(productos.tipos_id)){
            p.push(productos);
        }
        return p;
    }
    
    function buscarTipo(item,param){
        var result = [];
        if(typeof(param.tipos_id)!=='undefined'){
            for(var i in item){
                //Ver los productos
                var cat = item[i];
                var pro = [];
                for(var k in item[i].productos){                

                        res = _buscarTipo(item[i].productos[k],param.tipos_id);
                        if(res.length>0){
                            pro.push(res[0]);
                        }                
                }
                if(pro.length>0){
                    cat.productos = pro;
                    result.push(cat);               
                }
            }
        }else{
            result = item;
        }
        return result;
    }        
    //Destinatarios
    function _buscarFuncion(productos,search){
        search = search.toLowerCase();
        var p = [];
        var e= eval('productos.'+search);
        if(parseInt(e)===1){
            p.push(productos);
        }
        return p;
    }
    
    function buscarFuncion(item,param){
        var result = [];
        if(typeof(param.beverage_funciones_nombre)!=='undefined'){
            for(var i in item){
                //Ver los productos
                var cat = item[i];
                var pro = [];
                for(var k in item[i].productos){                                
                        res = _buscarFuncion(item[i].productos[k],param.beverage_funciones_nombre);
                        if(res.length>0){
                            pro.push(res[0]);
                        }                
                }
                if(pro.length>0){
                    cat.productos = pro;
                    result.push(cat);               
                }
            }
        }else{
            result = item;
        }
        return result;
    }
    //Clasificacion
    function _buscarClasificacion(productos,search){
        var p = [];        
        if(parseInt(search)===parseInt(productos.clasificacion_id)){
            p.push(productos);
        }
        return p;
    }
    
    function buscarClasificacion(item,param){
        var result = [];
        if(typeof(param.clasificacion_nombre)!=='undefined'){
            for(var i in item){
                //Ver los productos
                var cat = item[i];
                var pro = [];
                for(var k in item[i].productos){                

                        res = _buscarClasificacion(item[i].productos[k],param.clasificacion_nombre);
                        if(res.length>0){
                            pro.push(res[0]);
                        }                
                }
                if(pro.length>0){
                    cat.productos = pro;
                    result.push(cat);               
                }
            }
        }else{
            result = item;
        }
        return result;
    }        
    
    //parametro
    function _buscarParametro(productos,search){
        var p = [];
        var n = productos.beverage_nombre.toLowerCase();
        var s = search.toLowerCase();
        if(n.search(s)>=0){
            p.push(productos);
        }
        return p;
    }
    
    function buscarParametro(item,param){
        var result = [];
        if(typeof(param.nombre)!=='undefined'){
            
            for(var i in item){
                //Ver los productos
                var cat = item[i];
                var pro = [];
                for(var k in item[i].productos){                
                    res = _buscarParametro(item[i].productos[k],param.nombre);
                    if(res.length>0){
                        pro.push(res[0]);
                    }
                }
                if(pro.length>0){
                    cat.productos = pro;
                    result.push(cat);
                }
            }
            
        }else{
            return result = item;
        }
        return result;
    }
    
    return function(item,param) {
        var result = item;
        if(JSON.stringify(param)!=='{}'){            
            //Buscar destinatario
            result = buscarClasificacion(result,param);            
            //Buscar aplicaciones
            result = buscarFuncion(result,param);
            //Buscar parametro
            result = buscarParametro(result,param);
            //Tipos
            result = buscarTipo(result,param);
        }
        return result;
    };
});