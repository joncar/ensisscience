//var url = 'http://localhost/proyectos/pizzas/api/';
//var ipServer = 'http://localhost:3001'; //Ip del servidor socket        
var url = 'http://www.ensissciences.com/webnew/api/';
var ipServer = 'http://74.208.12.230:3000'; //Ip del servidor socket    
var urlS = 'http://74.208.12.230/';
var myPopup;
angular.module('starter.services', [])

.factory('Api', function() {
  var email = '';
  var password = '';
  var data = "";
  return {
    list:function(controller,data,$scope,$http,successFunction,operator){
        if(typeof(data)=='object'){
            s = '';            
            for(i in data){
                s+= 'search_field[]='+i+'&search_text[]='+data[i]+'&';
            }
            data = s;
            data += operator==undefined?'operator=where':'operator='+operator;
            data += '&idioma='+lang;
        }
        if($scope.loading!=undefined){
            $scope.loading('show');
        }
        $http({
            url:url+controller+'/json_list',
            method: "POST",
            data:data,
            transformRequest:angular.identity,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }           
          }).success(function(data){              
             if(successFunction!==undefined){
                 successFunction(data);
                if($scope.loading!=undefined){
                    $scope.loading('hide');
                }
             }
          })
        .catch(function(data, status, headers, config){ // <--- catch instead error

            //alert('{'+JSON.stringify(data)+'} {'+JSON.stringify(headers)+'}'); //contains the error message
            alert('Ha ocurrido un error intentando conectar con el servidor. Por favor intente más tarde');
        });
    },
    insert:function(controller,$scope,$http,successFunction){       
       if(!$scope.data){//Si ls datos son de un formulario
            d = document.getElementById('formreg');
            data = new FormData(d);  
        }//Si se envia el array
        else{
            data = new FormData();
            for(i in $scope.data){
              data.append(i,$scope.data[i]);
            }            
        }
        $scope.loading('show');     
       $http({
            url:url+controller+'/insert_validation',
            method: "POST",
            data:data,
            transformRequest: angular.identity,
            headers: {
              'Content-Type':undefined
            }           
          }).success(function(data){
              data = data.replace('<textarea>','');
              data = data.replace('</textarea>','');
              data = JSON.parse(data);
              if(data.success){
                  if(!$scope.data){//Si ls datos son de un formulario
                        d = document.getElementById('formreg');
                        data = new FormData(d);  
                  }//Si se envia el array
                  else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }            
                  }       
                  $http({
                        url:url+controller+'/insert',
                        method: "POST",
                        data:data,
                        transformRequest: angular.identity,
                        headers: {
                          'Content-Type':undefined
                        }           
                      }).success(function(data){
                          data = data.replace('<textarea>','');
                          data = data.replace('</textarea>','');
                          data = JSON.parse(data);                          
                          if(data.success){
                               if(successFunction!==undefined){
                                   successFunction(data);
                                   $scope.loading('hide');
                               }
                          }
                          else{
                             $scope.loading('hide');
                             $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                          }
                        })
                        .error(function(){
                            alert('Ha ocurrido un error interno, contacte con un administrador');
                        });
              }
              else{
                  $scope.loading('hide');
                  $scope.showAlert('Ocurrio un error al añadir',data.error_message);
              }
          })
          .error(function(data){
              alert('Ha ocurrido un error interno, contacte con un administrador');
          });
        },
        
        update:function(controller,id,$scope,$http,successFunction){
        if(!$scope.data){//Si ls datos son de un formulario
          d = document.getElementById('formreg');
          data = new FormData(d);
        }//Si se envia el array
        else{
          data = new FormData();
          for(i in $scope.data){
            data.append(i,$scope.data[i]);
          }
        }
        $scope.loading('show');            
        $http({
             url:url+controller+'/update_validation/'+id,
             method: "POST",
             data:data,
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined
             }           
           }).success(function(data){
               data = data.replace('<textarea>','');
               data = data.replace('</textarea>','');
               data = JSON.parse(data);
               if(data.success){
                    if(!$scope.data){//Si ls datos son de un formulario
                      d = document.getElementById('formreg');
                      data = new FormData(d);
                    }//Si se envia el array
                    else{
                      data = new FormData();
                      for(i in $scope.data){
                        data.append(i,$scope.data[i]);
                      }
                    }       
                   $http({
                         url:url+controller+'/update/'+id,
                         method: "POST",
                         data:data,
                         transformRequest: angular.identity,
                         headers: {
                           'Content-Type':undefined
                         }           
                       }).success(function(data){
                           data = data.replace('<textarea>','');
                           data = data.replace('</textarea>','');
                           data = JSON.parse(data);                          
                           if(data.success){
                                if(successFunction!==undefined){
                                   successFunction(data);
                                   $scope.loading('hide');
                                }
                           }
                           else{
                              $scope.loading('hide');
                              $scope.showAlert('Ocurrio un error al añadir','Ocurrio un error al añadir');
                           }
                    });
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al añadir',data.error_message);
               }
          });
        },
        
        deleterow:function(controller,id,$scope,$http,successFunction){        
        $scope.loading('show');
        $http({
             url:url+controller+'/delete/'+id,
             method: "GET",
             data:'',
             transformRequest: angular.identity,
             headers: {
               'Content-Type':undefined
             }           
           }).success(function(data){
               
               if(data.success){
                    if(successFunction!==undefined){
                        successFunction(data);
                        $scope.loading('hide');
                     }
               }
               else{
                   $scope.loading('hide');
                   $scope.showAlert('Ocurrio un error al eliminar',data.error_message);
               }
          });
        },

        initInterface:function($scope){

          return $scope;
        },

        query:function(controller,$scope,$http,successFunction){
            if(!$scope.data){//Si ls datos son de un formulario
              d = document.getElementById('formreg');
              data = new FormData(d);
            }
            else{
                data = new FormData();
                for(i in $scope.data){
                  data.append(i,$scope.data[i]);
                }
                data.append('idioma',lang);
            }
            
            if($scope.loading!=undefined){
                $scope.loading('show');
            }
            $http({
                 url:url+controller,
                 method: "POST",
                 data:data,
                 transformRequest: angular.identity,
                 headers: {
                   'Content-Type':undefined
                 }           
               }).success(function(data){
                    if(successFunction!==undefined){
                        successFunction(data);
                        if($scope.loading!=undefined){
                            $scope.loading('hide');
                        }
                     }
              }).error(function(data){
                  $scope.loading('hide');
                  $scope.showAlert('Ocurrio un error al enviar los datos',data);
              });
        }
    }   
})

.factory('UI', function() {
    return {    
        showPopupBox:function($ionicPopup,$scope,message,title,buttonAccept){
            $scope.data = {}
            // An elaborate, custom popup
              myPopup = $ionicPopup.show({
              template: message,
              title: title,              
              scope: $scope,
              buttons: [
                { 
                 text: 'Cancelar',
                 onTap:function(e){
                     myPopup.close();
                 }                 
                },
                buttonAccept
              ]
            });
            myPopup.then(function(res) {
              console.log('Tapped!', res);
            });
        },
        getShowAlert:function($ionicPopup,onclick,onshow){            
            return function(title,template){
                var alertPopup = $ionicPopup.alert({
                  title: title,
                  template: template
                });
                
                alertPopup.then(function(res) {
                    if(typeof(onclick)!=='undefined'){
                        onclick();
                    }
                    alertPopup.close();
                });
                if(typeof(onshow)!=='undefined'){
                    onshow(alertPopup);
                }
            };
        },
        getLoadingBox:function($ionicLoading,message){
            message = message==undefined?'Cargando...':message;
            return function(attr){            
                attr=='hide'?$ionicLoading.hide():$ionicLoading.show({template: message});
            };
        }, 
        
        getConfirmBox:function($ionicPopup,callback) {
           return function(title,template,acceptFunction,denyFunction){
               var confirmPopup = $ionicPopup.confirm({
                    title: title,
                    template: template
                  });
                  confirmPopup.then(function(res) {
                    if(res) {
                      if(acceptFunction)acceptFunction();
                    } else {
                      if(denyFunction)denyFunction();
                    }
                  });
                  if(typeof(callback)!=='undefined'){
                    callback(confirmPopup);
                  }
            };
        },
        getLocationName:function(lat,lon,callback){
            var geocoder = new google.maps.Geocoder;
            geocoder.geocode({'latLng': new google.maps.LatLng(lat,lon)}, function(results, status) {
                //calle                                 
                callback(results[0]);
            });
        },
        getModalBox:function($ionicModal,template,$scope,callback){
            $ionicModal.fromTemplateUrl(template, {//Invitar personas
                scope: $scope,
                animation: 'slide-in-up'
              }).then(function(modal) {
                $scope.modal = modal;
                if(typeof(callback)!=='undefined'){
                    callback(modal);
                }
              });

            $scope.toggleModal = function(attr) {
              if(attr==undefined || attr=='show'){
                  $scope.modal.show();
              }
              else{
                  $scope.modal.hide();    
              }
            };            
            
            $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {                
                $scope.toggleModal('hide');
            });
        },
    };
})

.factory('User', function() {  
  return {
        setData:function(data){            
            localStorage.user = data.id;
            localStorage.nombre = data.nombre_repartidor;            
            localStorage.email = data.email;            
            localStorage.telefono = data.telefono;
            localStorage.foto = urlS+'pizzasapp/images/repartidores/'+data.foto;
            localStorage.placa = data.placa;
            localStorage.rank = data.calificacion;
        },
        
        getData:function($scope){
            $scope.user = localStorage.user;
            $scope.email = localStorage.email;        
            $scope.nombre = localStorage.nombre;                  
            $scope.telefono = localStorage.telefono;                  
            $scope.foto = localStorage.foto;
            $scope.placa = localStorage.placa;
            $scope.rank = localStorage.rank;
            return $scope;
        },
        
        cleanData:function(){
            localStorage.removeItem('user');
            localStorage.removeItem('nombre');            
            localStorage.removeItem('email');            
            localStorage.removeItem('telefono');            
            localStorage.removeItem('foto');            
            localStorage.removeItem('placa');            
            localStorage.removeItem('rank');
            localStorage.removeItem('lat');
            localStorage.removeItem('lon');
        }
    }  
})

.factory('Group', function() {  
  return {
     list:[],    
     add:function(cliente){
        var pos = this.getPosition(cliente.id);
        if(pos===null)
        this.list.push(cliente);
        else this.list[pos] = cliente;
    },    
    remove:function(cliente){
        if(cliente!==undefined){
            var aux = [];
            for(var i in this.list){
                if(this.list[i].id!==cliente.id){
                    aux.push(this.list[i]);
                }
            }
            this.list = aux;
        }
    },
    update:function(cliente){
        for(var i in this.list){
            if(this.list[i].id===cliente.id){
                this.list[i] = cliente;
            }
        }
    },
    getFromId:function(id){
        var cliente = null;
        for(var i in this.list){
            if(this.list[i].id===id){
                cliente = this.list[i];
            }
        }
        return cliente;
    },    
    getPosition:function(id){
        var cliente = null;
        for(var i in this.list){
            if(this.list[i].id===id){
                cliente = i;
            }
        }
        return cliente;
    },
    save:function(name){
        localStorage[name] = JSON.stringify(this.list);
    },
    load:function(name){
        this.list = localStorage[name];
        this.list = typeof(this.list)!=='undefined'?JSON.parse(this.list):[];
        if(this.list===undefined){
            this.list = [];
        }
    },
    erase:function(name){
        localStorage.removeItem(name);
    }
  };
})

.factory('SocketConnection', function() {  
  return {       
        /*ipServer: ipServer,        
        WebSocketOpen:function($scope){
            if(typeof(io)!=='undefined'){
                ws = io.connect(this.ipServer,{'force new connection': true});
                ws.on('connect',function(){
                    $scope.connected();
                    ws.on('onopen', function() {  
                       if(ws.userID!=undefined){
                           ws.connectID('connectID',{id:ws.userID});
                       }
                       console.log('Conexion con el socket establecida');
                    });
                });

                ws.on('disconnect',function(){
                    $scope.disconnected();
                });
            }
            //Eventos personalizados, Borrar a la hora de reutilizar en otro proyecto                                   
        },
        
        connectID:function(userID){ //Borrar solo si es necesario Esta función envia el ID del usuario al servidor
            if(typeof(ws)!=='undefined'){
                ws.connectID = function(evt,datos){
                    datos.type = 'repartidor';
                    ws.emit(evt,datos);
                }
                ws.connectID('connectID',{id:userID});
                ws.userID = userID;
            }
        },*/
    };
});