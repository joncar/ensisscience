controllers.controller('Busqueda', function($scope,$rootScope,$http,Api) {
    $scope.urlImg = ServerUrl+'img/foods/';
    $scope.urlIco = ServerUrl+'img/gamas/';
    //Traer destinatarios
    $scope.gamas = [];
    $scope.loadingSpinner = true;
    $scope.showed = '';
    //Api.search = {gamas_id:1};
    
    $scope.consultar = function(){
        $scope.data = Api.search;
        Api.query('buscarFood',$scope,$http,function(data){
            $scope.productos = data;
            if(localStorage.gamas!==undefined){
                $scope.armar();
            }else{
                Api.list('gamas',{},$scope,$http,function(data){
                    $scope.gamas = [];
                    for(var i=data.length-1;i>=0;i--){
                        data[i].productos = JSON.parse(data[i].productos);
                        $scope.gamas.push(data[i]);
                    }
                    //Cachear los datos
                    Api.gamas = $scope.gamas;
                    localStorage.gamas = JSON.stringify($scope.gamas);
                    $scope.armar();
                });
            }
        });
    };
    
    $scope.armar = function(){        
        
        $scope.gamas = JSON.parse(localStorage.gamas);
        for(var i in $scope.gamas){
            $scope.gamas[i].productos = [];
            for(var k in $scope.productos){
                if($scope.productos[k].gamas_id===$scope.gamas[i].id){
                    $scope.gamas[i].productos.push($scope.productos[k]);
                }
            }
        }
        console.log($scope.gamas);
        $scope.loadingSpinner = false;        
    };
    
    if(Api.search!==undefined){
        console.log(Api.search);
        $scope.consultar(Api.search);
    }else{
        document.location.href="#/tab/tabs/destinado";  
    }
    
     $scope.shownGroup = [];
    $scope.toggleGroup = function(group) {
        if($scope.isGroupShown(group)){
            $scope.shownGroup[group.id] = false;
        }else{
            $scope.shownGroup[group.id] = true;
        }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup[group.id]?true:false;
    };
    
    $scope.searchInfo = {};
    $scope.$on('search',function(evt,data){
        $scope.consultar();
    });
    
    $scope.producto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };       
});