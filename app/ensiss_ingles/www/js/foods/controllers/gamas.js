controllers.controller('Gamas', function($scope,$rootScope,$http,Api) {
    $scope.urlImg = ServerUrl+'img/foods/';
    $scope.urlIco = ServerUrl+'img/gamas/';
    //Traer destinatarios
    $scope.gamas = [];
    $scope.loadingSpinner = true;
    $scope.showed = '';
    
    $scope.consultar = function(){
        Api.list('gamas',{},$scope,$http,function(data){
            $scope.gamas = [];
            for(var i=data.length-1;i>=0;i--){
                data[i].productos = JSON.parse(data[i].productos);
                $scope.gamas.push(data[i]);
            }
            //Cachear los datos
            Api.gamas = $scope.gamas;
            localStorage.gamas = JSON.stringify($scope.gamas);
            $scope.loadingSpinner = false;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };    
    if(localStorage.gamas===undefined){
        $scope.consultar();
    }else{
        $scope.gamas = JSON.parse(localStorage.gamas);
        Api.gamas = $scope.gamas;
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loadingSpinner = false;
    }
    
    $scope.shownGroup = [];
    $scope.toggleGroup = function(group) {
        if($scope.isGroupShown(group)){
            $scope.shownGroup[group.id] = false;
        }else{
            $scope.shownGroup[group.id] = true;
        }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup[group.id]?true:false;
    };
    
    $scope.searchInfo = {};
    $scope.$on('search',function(evt,data){
        $scope.searchInfo = data;
    });
    
    $scope.producto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };
});