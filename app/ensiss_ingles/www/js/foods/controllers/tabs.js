controllers.controller('Tabs', function($scope,$state) {           
    $scope.tabSeleccionado = 1;
    $scope.goDestinado = function(){
      $scope.tabSeleccionado = 1;
      $state.go('tab.tabs.destinado');
    };
    $scope.goAplicaciones = function(){
      $scope.tabSeleccionado = 2;
      $state.go('tab.tabs.aplicaciones');  
    };
    $scope.goGamas = function(){
      $scope.tabSeleccionado = 3;
      $state.go('tab.tabs.gamas');  
    };
    
    $scope.filter = '';        
});