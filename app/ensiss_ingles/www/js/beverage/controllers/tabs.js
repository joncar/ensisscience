controllers.controller('Tabs', function($scope,$rootScope,$state) {               
    $scope.tabSeleccionado = 1;
    $scope.goVinos = function(){
      $scope.tabSeleccionado = 1;
      $state.go('tab.tabs.vinos');
    };
    $scope.goCervezas = function(){
      $scope.tabSeleccionado = 2;
      $state.go('tab.tabs.cervezas');  
    };
    
    $scope.goFunciones = function(){
      $scope.tabSeleccionado = 3;
      $state.go('tab.tabs.funciones');  
    };
    
    $scope.$on('mostrarSearch',function(){
        $scope.mostrarSearch();
    });
    
    $scope.filter = '';        
});