controllers.controller('Favoritos', function($scope,$rootScope,$http,Api,Group) {
    $scope.urlImg = ServerUrl+'img/foods/';
    $scope.urlIco = ServerUrl+'img/clasificaciones/';
    Group.load('foodFav');
    $scope.favoritos = [{
            clasificacion_nombre:'Favoritos',
            productos:Group.list
    }];
    $scope.producto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };
});