controllers.controller('Productos', function($scope,$state,$stateParams,Api,Group) {
    $scope.urlImg = ServerUrl+'img/foods/';
    $scope.urlIco = ServerUrl+'img/aplicaciones/';
    $scope.viewed = 'vista';
    Group.load('foodFav');                
    $scope.relacionados = [];
    if($stateParams.id===undefined || (localStorage.aplicaciones===undefined && localStorage.destinatarios===undefined)){
        document.location.href="#foods.html";
    }else{
        //Buscar coleccion
        var list = localStorage.destinatarios===undefined?localStorage.aplicaciones:localStorage.destinatarios;
        list = JSON.parse(list);
        var item = '';
        for(var i in list){
            //Buscar el producto
            if(item===''){
                $scope.relacionados = list[i];
                for(var k in list[i].productos){
                    if(list[i].productos[k].id===$stateParams.id){
                        item = list[i].productos[k];
                    }
                }
            }
        }
        if(item===''){
           document.location.href="#foods.html";
        }else{
            $scope.producto = item;
            var relacionados = [];
            for(i in $scope.relacionados.productos){
                if(relacionados.length<9){
                    relacionados.push($scope.relacionados.productos[i]);
                }
            }
            $scope.relacionados.productos = relacionados;            
            $scope.iconColor = Group.getFromId(item.id)!==null?'red':'black';   
            var des = $scope.producto.descripcion;
            des = des.replace('//','/');
            $scope.producto.descripcion = des.replace('/webnew/',ServerUrl);            
        }
    }
    
    $scope.next = function(){
      var id = parseInt($stateParams.id)+1;  
      $scope.goProducto(id);
    };
    
    $scope.goProducto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };
    
    $scope.share = function(){
        var url = 'http://ensissciences.com/food/'+$scope.producto.id+'-producto.html';
        window.plugins.socialsharing.share($scope.producto.foods_nombre,null,null,url);
    };
    
    $scope.addFav = function(producto){
            if(Group.getFromId(item.id)!==null){
                Group.remove(producto);   
                $scope.iconColor = 'black';
            }else{
                Group.add(producto);   
                $scope.iconColor = 'red';
                alert('Este producto se añadido a tu lista de favoritos que podrás consultar en el menú lateral');
            }
             Group.save('foodFav');
    };
});