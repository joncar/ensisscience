controllers.controller('Contacto', function($scope,$rootScope,$http,Api) {
    $scope.contacto = function(datos){
        if(datos===undefined){
            alert('Por favor complete los datos, para poder contactarlo');
        }else{
            $scope.data = datos;
            Api.query('contactar',$scope,$http,function(data){
                alert('En breve le contactaremos, gracias por comunicarse con nosotros.');
            });
        }
    };
});