controllers.controller('Productos', function($scope,$rootScope,$state,$stateParams,Api,Group) {
    $scope.urlImg = ServerUrl+'img/beverages/';
    $scope.urlIco = ServerUrl+'img/clasificaciones/';
    $scope.urlIco2 = ServerUrl+'img/beverage_funciones/';
    Group.load('beverageFav');                
    $scope.viewed = 'vista';
    $scope.relacionados = [];
    if($stateParams.id===undefined || (localStorage.vinos===undefined && localStorage.cervezas===undefined) || localStorage.funcionesSearch===undefined){
        document.location.href="#beverage.html";
    }else{
        //Buscar coleccion
        var item = '';
        if(localStorage.vinos!==undefined){
            var p = JSON.parse(localStorage.vinos);            
            for(var i in p){
                $scope.relacionados = p[i];
                for(var k in p[i].productos){
                    if(p[i].productos[k].id===$stateParams.id){
                        item = p[i].productos[k];
                    }
                }
            }
        }
        if(item==='' && localStorage.cervezas!==undefined){
            var p = JSON.parse(localStorage.cervezas);
            for(var i in p){
                $scope.relacionados = p[i];
                for(var k in p[i].productos){
                    if(p[i].productos[k].id===$stateParams.id){
                        item = p[i].productos[k];
                    }
                }
            }
        }        
        
        if(item===''){
           document.location.href="#beverage.html";
        }else{
            $scope.producto = item;
            
            $scope.funciones = [];
            var f = JSON.parse(localStorage.funcionesSearch);
            for(var i in f){
                var valor = eval('$scope.producto.'+f[i].url);
                if(parseInt(valor)===1){
                    $scope.funciones.push(f[i]);
                }
            }
        
            var relacionados = [];
            for(i in $scope.relacionados.productos){
                if(relacionados.length<9){
                    relacionados.push($scope.relacionados.productos[i]);
                }
            }
            $scope.relacionados.productos = relacionados;
            $scope.iconColor = Group.getFromId(item.id)!==null?'red':'black';
            var des = $scope.producto.descripcion;
            des = des.replace('//','/');
            $scope.producto.descripcion = des.replace('/webnew/',ServerUrl);
        }
    }
    
    $scope.next = function(){
      var id = parseInt($stateParams.id)+1;  
      $scope.goProducto(id);
    };
    
    $scope.goProducto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };
    
    $scope.share = function(){
        var url = 'http://ensissciences.com/food/'+$scope.producto.id+'-producto.html';
        window.plugins.socialsharing.share($scope.producto.beverage_nombre,null,null,url);
    };
    
    $scope.addFav = function(producto){
            if(Group.getFromId(item.id)!==null){
                Group.remove(producto);   
                $scope.iconColor = 'black';
            }else{
                Group.add(producto);   
                $scope.iconColor = 'red';
                alert('Este producto se añadido a tu lista de favoritos que podrás consultar en el menú lateral');
            }
             Group.save('beverageFav');
    };
});