controllers.controller('Busqueda', function($scope,$rootScope,$http,Api) {
    $scope.urlImg = ServerUrl+'img/beverages/';
    $scope.urlIco = ServerUrl+'img/beverage_funciones/';
    //Traer destinatarios
    $scope.funciones = [];
    $scope.loadingSpinner = true;
    $scope.showed = '';
    //Api.search = {tipos_id:1};
   
    $scope.consultar = function(){
        
        $scope.data = Api.search;
        Api.query('buscarBeverage',$scope,$http,function(data){
            $scope.productos = data;
            if(localStorage.funciones!==undefined){
                $scope.armar();
            }else{
                Api.list('funciones',{},$scope,$http,function(data){
                    $scope.funciones = [];
                    for(var i=data.length-1;i>=0;i--){
                        data[i].productos = JSON.parse(data[i].productos);
                        $scope.funciones.push(data[i]);
                    }
                    //Cachear los datos
                    Api.funciones = $scope.funciones;
                    localStorage.funciones = JSON.stringify($scope.funciones);
                    $scope.armar();
                });
            }
        });
    };
    
    $scope.armar = function(){        
        
        $scope.funciones = JSON.parse(localStorage.funciones);
        for(var i in $scope.funciones){
            $scope.funciones[i].productos = [];
            for(var k in $scope.productos){
                if($scope.productos[k][$scope.funciones[i].url]==='1'){
                    $scope.funciones[i].productos.push($scope.productos[k]);
                }
            }
        }
        console.log($scope.gamas);
        $scope.loadingSpinner = false;        
    };
    
    if(Api.search!==undefined){
        console.log(Api.search);
        $scope.consultar(Api.search);
    }else{
        document.location.href="#/tab/tabs/vinos";  
    }
    
     $scope.shownGroup = [];
    $scope.toggleGroup = function(group) {
        if($scope.isGroupShown(group)){
            $scope.shownGroup[group.id] = false;
        }else{
            $scope.shownGroup[group.id] = true;
        }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup[group.id]?true:false;
    };
    
    $scope.searchInfo = {};
    $scope.$on('search',function(evt,data){
        $scope.consultar();
    });
    
    $scope.producto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };       
});