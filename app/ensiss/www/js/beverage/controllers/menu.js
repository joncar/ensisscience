var controllers = angular.module('starter.controllers', []);
controllers.controller('Menu', function($scope,$rootScope,$ionicSideMenuDelegate,$ionicModal,$http,UI,Api) {     
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };    
    $scope.search = function(){
        $rootScope.$broadcast('mostrarSearch');
    };
    
    //buscar clasificacionesSearch
    if(localStorage.clasificacionesSearch===undefined){
        Api.list('clasificacionesSearch',{},$scope,$http,function(data){
            Api.clasificacionesSearch = data;
            localStorage.clasificacionesSearch = JSON.stringify(data);
            $scope.clasificacionesSearch = Api.clasificacionesSearch;
        });
    }else{
        Api.clasificacionesSearch = JSON.parse(localStorage.clasificacionesSearch);
        $scope.clasificacionesSearch = Api.clasificacionesSearch;
    }
    //buscar funciones
    if(localStorage.funcionesSearch===undefined){                
        Api.list('funcionesSearch',{},$scope,$http,function(data){            
            localStorage.funcionesSearch = JSON.stringify(data);
            $scope.funcionesSearch = data;
        });
    }else{
        $scope.funcionesSearch = JSON.parse(localStorage.funcionesSearch);
    }
    
    $scope.search = {};
    $scope.buscar = function(search){
        $scope.closeModal();
        Api.search = search;
        $scope.search = {};
        document.location.href="#/tab/tabs/busqueda";
        $rootScope.$broadcast('search',search);
    };
    $scope.mostrarSearch = function(){
        UI.getModalBox($ionicModal,'templates/beverage/modals/search.html',$scope,function(modal){
            $scope.closeModal = function(){modal.hide();};
            modal.show();
        });
    };
});