MapApp.filter('productSearch', function() {
    //Destinatarios
    function _buscarDestinatario(productos,search){
        var p = [];
        for(var i in productos.destinatarios){
            if(productos.destinatarios[i].destinatarios_nombre===search){
                p.push(productos);
            }
        }        
        return p;
    }
    
    function buscarDestinatario(item,param){
        var result = [];
        if(typeof(param.destinatarios_nombre)!=='undefined'){
            for(var i in item){
                //Ver los productos
                var cat = item[i];
                var pro = [];
                for(var k in item[i].productos){                                
                        res = _buscarDestinatario(item[i].productos[k],param.destinatarios_nombre);
                        if(res.length>0){
                            pro.push(res[0]);
                        }                
                }
                if(pro.length>0){
                    cat.productos = pro;
                    result.push(cat);               
                }
            }
        }else{
            result = item;
        }
        return result;
    }
    //Aplicaciones
    function _buscarAplicacion(productos,search){
        var p = [];
        for(var i in productos.aplicaciones){
            if(productos.aplicaciones[i].aplicaciones_nombre===search){
                p.push(productos);
            }
        }        
        return p;
    }
    
    function buscarAplicacion(item,param){
        var result = [];
        if(typeof(param.aplicaciones_nombre)!=='undefined'){
            for(var i in item){
                //Ver los productos
                var cat = item[i];
                var pro = [];
                for(var k in item[i].productos){                

                        res = _buscarAplicacion(item[i].productos[k],param.aplicaciones_nombre);
                        if(res.length>0){
                            pro.push(res[0]);
                        }                
                }
                if(pro.length>0){
                    cat.productos = pro;
                    result.push(cat);               
                }
            }
        }else{
            result = item;
        }
        return result;
    }
    
    //Gamas
    function _buscarGama(productos,search){
        var p = [];
        if(parseInt(productos.gamas_id)===parseInt(search)){
            p.push(productos);
        }        
        return p;
    }
    function buscarGamas(item,param){
        var result = [];
        if(typeof(param.gamas_id)!=='undefined'){
            for(var i in item){
                //Ver los productos
                var cat = item[i];
                var pro = [];
                for(var k in item[i].productos){                
                    res = _buscarGama(item[i].productos[k],param.gamas_id);
                    if(res.length>0){
                        pro.push(res[0]);
                    }
                }
                if(pro.length>0){
                    cat.productos = pro;
                    result.push(cat);
                }
            }
        }else{
            result = item;
        }
        return result;
    }
    
    //parametro
    function _buscarParametro(productos,search){
        var p = [];
        var n = productos.foods_nombre.toLowerCase();
        var s = search.toLowerCase();
        if(n.search(s)>=0){
            p.push(productos);
        }
        return p;
    }
    
    function buscarParametro(item,param){
        var result = [];
        if(typeof(param.nombre)!=='undefined'){
            
            for(var i in item){
                //Ver los productos
                var cat = item[i];
                var pro = [];
                for(var k in item[i].productos){                
                    res = _buscarParametro(item[i].productos[k],param.nombre);
                    if(res.length>0){
                        pro.push(res[0]);
                    }
                }
                if(pro.length>0){
                    cat.productos = pro;
                    result.push(cat);
                }
            }
            
        }else{
            return result = item;
        }
        return result;
    }
    
    return function(item,param) {
        var result = item;
        if(JSON.stringify(param)!=='{}'){            
            //Buscar destinatario
            result = buscarDestinatario(result,param);            
            //Buscar aplicaciones
            result = buscarAplicacion(result,param);
            //Buscar gamas
            result = buscarGamas(result,param);
            //Buscar parametro
            result = buscarParametro(result,param);
        }
        return result;
    };
});