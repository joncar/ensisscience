// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var MapApp = angular.module('starter', ['ionic', 'starter.controllers', 'starter.services'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs).
    // The reason we default this to hidden is that native apps don't usually show an accessory bar, at
    // least on iOS. It's a dead giveaway that an app is using a Web View. However, it's sometimes
    // useful especially with forms, though we would prefer giving the user a little more room
    // to interact with the app.
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // Set the statusbar to use the default style, tweak this to
      // remove the status bar on iOS or change it to use white instead of dark colors.
      StatusBar.styleDefault();
    }
  });
})
.config(function($ionicConfigProvider){
    $ionicConfigProvider.backButton.previousTitleText(false);
})
.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    cache:false,
    abstract: true,    
    templateUrl: 'templates/en/foods/tabs.html',
    controller:'Menu'
  })

  // Each tab has its own nav history stack:

  .state('tab.tabs', {
    url: '/tabs',
    cache:false,
    views: {
      'tab-tabs': {
        templateUrl: 'templates/en/foods/main.html',
        controller: 'Tabs'
      }
    }
  })
  
  .state('tab.tabs.destinado', {
    url: '/destinado',
    cache:false,
    views: {
      'tab-destinado': {
        templateUrl: 'templates/en/foods/destinado.html',
        controller: 'Destinado'
      }
    }
  })
  
  .state('tab.tabs.aplicaciones', {
    url: '/aplicaciones',
    cache:false,
    views: {
      'tab-destinado': {
        templateUrl: 'templates/en/foods/aplicaciones.html',
        controller: 'Aplicaciones'
      }
    }
  })
  
  .state('tab.tabs.gamas', {
    url: '/gamas',
    cache:false,
    views: {
      'tab-destinado': {
        templateUrl: 'templates/en/foods/gamas.html',
        controller: 'Gamas'
      }
    }
  })
  
  .state('tab.tabs.busqueda', {
    url: '/busqueda',
    cache:false,
    views: {
      'tab-destinado': {
        templateUrl: 'templates/en/foods/busqueda.html',
        controller: 'Busqueda'
      }
    }
  })
  
  .state('tab.productos', {
    url: '/productos/:id',
    cache:false,
    views: {
      'tab-tabs': {
        templateUrl: 'templates/en/foods/productos.html',
        controller: 'Productos'
      }
    }
  })
  
    .state('tab.contacto', {
    url: '/contacto',
    cache:false,
    views: {
      'tab-tabs': {
        templateUrl: 'templates/en/contacto.html',
        controller: 'Contacto'
      }
    }
  })
  
  .state('tab.favoritos', {
    url: '/favoritos',
    cache:false,
    views: {
      'tab-tabs': {
        templateUrl: 'templates/en/foods/favoritos.html',
        controller: 'Favoritos'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/tabs/destinado');

});
;
