var controllers = angular.module('starter.controllers', []);
controllers.controller('Menu', function($scope,UI,$ionicModal,$http,$rootScope,$ionicSideMenuDelegate,Api) {         
    
    $scope.showMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };  
    
    //buscar gamas
    if(localStorage.gamasSearch===undefined){
        Api.list('gamasSearch',{},$scope,$http,function(data){
            Api.gamasSearch = data;
            localStorage.gamasSearch = JSON.stringify(data);
            $scope.gamas = Api.gamasSearch;
        });
    }else{
        Api.gamasSearch = JSON.parse(localStorage.gamasSearch);
        $scope.gamas = Api.gamasSearch;
    }
    //buscar aplicaciones
    if(localStorage.aplicacionesSearch===undefined){                
        Api.list('aplicacionesSearch',{},$scope,$http,function(data){            
            localStorage.aplicacionesSearch = JSON.stringify(data);
            $scope.aplicaciones = data;
        });
    }else{
        $scope.aplicaciones = JSON.parse(localStorage.aplicacionesSearch);
    }
    //buscar destinatarios
    //buscar aplicaciones
    if(localStorage.destinatariosSearch===undefined){                
        Api.list('destinatariosSearch',{},$scope,$http,function(data){            
            localStorage.destinatariosSearch = JSON.stringify(data);
            $scope.destinatarios = data;
        });
    }else{
        $scope.destinatarios = JSON.parse(localStorage.destinatariosSearch);
    }
    
    $scope.mostrarSearch = function(){
        UI.getModalBox($ionicModal,'templates/en/foods/modals/search.html',$scope,function(modal){
            $scope.closeModal = function(){modal.hide();};
            modal.show();
        });
    };
    
    $scope.search = {};
    $scope.buscar = function(search){
        $scope.closeModal();
        Api.search = search;
        $scope.search = {};
        document.location.href="#/tab/tabs/busqueda";
        $rootScope.$broadcast('search',search);
    };    
    
});