controllers.controller('Destinado', function($scope,$rootScope,$http,Api) {
    $scope.urlImg = ServerUrl+'img/foods/';
    //Traer destinatarios
    $scope.destinatarios = [];
    $scope.loadingSpinner = true;
    $scope.showed = '';
    
    $scope.consultar = function(){
        Api.list('destinatarios',{},$scope,$http,function(data){
            $scope.destinatarios = [];
            for(var i=data.length-1;i>=0;i--){
                data[i].productos = JSON.parse(data[i].productos);
                $scope.destinatarios.push(data[i]);
            }
            //Cachear los datos
            Api.destinatarios = $scope.destinatarios;
            localStorage.destinatarios = JSON.stringify($scope.destinatarios);
            $scope.loadingSpinner = false;
            $scope.$broadcast('scroll.refreshComplete');
        });
    };    
    if(localStorage.destinatarios===undefined){
        $scope.consultar();
    }else{
        $scope.destinatarios = JSON.parse(localStorage.destinatarios);
        Api.destinatarios = $scope.destinatarios;
        $scope.$broadcast('scroll.refreshComplete');
        $scope.loadingSpinner = false;
    }
    
    $scope.shownGroup = [];
    $scope.toggleGroup = function(group) {        
        if($scope.isGroupShown(group)){
            $scope.shownGroup[group.id] = false;
        }else{
            $scope.shownGroup[group.id] = true;
        }
    };
    $scope.isGroupShown = function(group) {
      return $scope.shownGroup[group.id]?true:false;
    };
    
    $scope.searchInfo = {};
    $scope.$on('search',function(evt,data){
        $scope.searchInfo = data;
    });
    
    $scope.producto = function(id){
      document.location.href="#/tab/productos/"+id;  
    };
});